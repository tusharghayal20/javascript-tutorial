//To run => node app.js
/******************************************************************************
*                             CallBack
/******************************************************************************/
/*
console.log("=======================  Normal Function  ===========================")
function login(user, pass) {
    console.log("In Function login");
    setTimeout(()=>{
        console.log("Return Function login");
        return {Email:user,Password:pass}
    },1000)
    console.log("Out Function login");
}
const loginValue = login("tushar@gmail.com",123456)
console.log(loginValue);// It will return Undefine because before getting responce from set time out we call it 
//To sokle this problem we use Callback / Promises / Async-await

console.log("=======================  CallBack  ===========================")
function loginCallBack(user, pass, Callback) {
    console.log("In Function loginCallBack");
    setTimeout(()=>{
        console.log("Return Function loginCallBack");
        Callback({Email:user,Password:pass})
    },1500)
    console.log("Out Function loginCallBack");
}


console.log("=======================  CallBack  ===========================")
function getUserVideo(email, Callback) {
    console.log("getUserVideo");
    setTimeout( ()=>{
        console.log("getUserVideo setTimeout",email);
        Callback(["video1","video2","video3","video4","video5",]);
    }, 1000)
}

function videoDetails(video, onSuccess, onFailure) {
    setTimeout( ()=>{
        video ? onSuccess("Video Detaile")  :onFailure("Error in geting Video Detail")
    }, 1000)
}


const loginValueCallBack = loginCallBack("tushar@gmail.com",123456, objectAnyName =>{
    console.log(objectAnyName);
    getUserVideo(objectAnyName.Email, (onSuccess)=>{

        videoDetails( false , onSuccess =>{
            console.log(onSuccess);
        },
        (onFailure)=> {
            console.log(onFailure);
        })
    })    
})
*/
/******************************************************************************
*                             Promise 
/******************************************************************************/

 //*************** */ A Promise executes as soon as it is created./****************** */
/*
let p = new Promise(function(resolve, reject) {
 console.log("Promise started");
 console.log("Promise is doing some important work...");
 console.log("Promise has completed, will resolve shortly");
 resolve("Promise resolved");
    //OR
 //reject("Error occurred");
});   
//We dont need to call promise For this behaviour and other 
//special use-cases we almost always wrap a promise inside a 
//function and whenever we want to execute the promise, we just call that function.
p.then(
    function(message) {
     console.log("Resolve: ", message);
    },
    function(error) {
     console.log("Reject: ", error);
    }
);






function doSomeStuff() {
    return new Promise(function(resolve, reject) {
        console.log("Promise started");
        console.log("Promise is doing some important work...");
        console.log(
        "Promise has completed, will resolve shortly"
        );
        resolve("Promise resolved");
    });
}

doSomeStuff().then(
    function(message) {
        console.log("Resolved: ", message);
    },
    function(error) {
        console.log("Rejected: ", error);
    }
);




let errorPromise = new Promise(function(resolve, reject) {
    console.log("Promise started");
    console.log("Promise is doing some important work...");
    console.log("Promise is now raising an exception.");
    throw "Exception raised";
    resolve("Error promise resolved");
});

errorPromise
    .then(
        function(message) {
        console.log("Success: ", message);
        },
        function(error) {
        console.log("Failure: ", error);
        }
    )
    .catch(function(error) {
        console.log("Exception: ", error);
    });
//If you provide a reject method, throwing an error will automatically call it.
//If you don’t provide a reject method but a catch method, throwing an error will call the catch method.
//Even if some exception is thrown not by your own code but some other library that you used within the 
//promise it will be handled by the catch function you provided.



function doSomething() {
    return new Promise(function(resolve, reject) {
        console.log("=======================");
        let count = 0;
        count += 1;
        resolve(count);
    });
}

function resolve(count) {
    console.log("====");
    return new Promise(function(resolve) {
     count += 1;
     resolve(count);
    });
   }
   
   doSomething()
    .then(resolve)
    .then(resolve)
    .then(function(count) {
     console.log("Count: ", count);
});

*/
//===================================================================================

// const promise = new Promise( (resolve, reject) =>{
//     setTimeout(()=>{
//         resolve({Name:"Tushar"})
//         reject(new Error("User Not login"))
//     }, 500);
// });

// promise.then( user => {
//     console.log(user);
// }).catch( err => {
//     console.log(err.message);
// })


// function login(email, password) {
//     return new Promise((resolve, reject) => {
//         setTimeout(()=>{
//             resolve({userEmail: email})
//         },1500)
//     });
// }


// console.log("=======================  CallBack  ===========================")
// function getUserVideo(email) {
//     return new Promise((resolve, reject) => {
//         setTimeout( ()=>{
//             resolve(["Video1","Video2"])
//        }, 500)
//     });
// }

// function videoDetails(video) {
//     return new Promise((resolve, reject)=>{
//         setTimeout( ()=>{
//              resolve("Video Detaile")
//         }, 500)
//     })
// }


// const loginValueCallBack = loginCallBack("tushar@gmail.com",123456, objectAnyName =>{
//     console.log(objectAnyName);
//     getUserVideo(objectAnyName.Email, (onSuccess)=>{

//         videoDetails( false , onSuccess =>{
//             console.log(onSuccess);
//         },
//         (onFailure)=> {
//             console.log(onFailure);
//         })
//     })    
// })

/*
login("tushar@gmail.com",123456)
.then(user => getUserVideo(user.email))
.then(videos =>{ console.log(videos),
    videoDetails(videos[0])})
.then(detail => console.log(detail))
*/

/* //with Promise
login("tushar@gmail.com",123456)
.then(user => getUserVideo(user.email))
.then(videos => videoDetails(videos[0]))
.then(detail => console.log(detail))// Output => Video Details
*/

 /*//With async
async function displaLogin(){
    try {
        const user = await login("tushar@gmail.com",123456);
        const videos = await getUserVideo(user.email)
        const detail = await videoDetails(videos[0])
        console.log(detail)// Output => Video Details
    } catch (error) {
        console.log(error);
        console.log("Error");
    }
}
displaLogin()

*/
//If yo want to fetch date from youtube and facebook at same time
// const yt = new Promise(resolve =>{
//     setTimeout(()=>{
//         console.log("getting youtube videos");
//         resolve({videos: [1, 2, 3, 4, 5]})
//     }, 1000);
// });


// const fb = new Promise(resolve =>{
//     setTimeout(()=>{
//         console.log("Getting data from facebook");
//         resolve({user:"Name"})
//     },5000);
// });

//When both data ids fetach
// Promise.all([yt, fb]).then( result => console.log(result))