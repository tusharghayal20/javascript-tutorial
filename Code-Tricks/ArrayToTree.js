const arr = [
    {
       "id": "12",
       "parentId": "0",
       "text": "Man",
       "level": "1",
       "children": null
    },
    {
       "id": "6",
       "parentId": "12",
       "text": "Boy",
       "level": "2",
       "children": null
    },
    {
       "id": "7",
       "parentId": "12",
       "text": "Other",
       "level": "2",
       "children": null
    },
    {
       "id": "9",
       "parentId": "0",
       "text": "Woman",
       "level": "1",
       "children": null
    },
    {
       "id": "11",
       "parentId": "9",
       "text": "Girl",
       "level": "2",
       "children": null
    }
 ];
 const listToTree = (arr = []) => {
    let map = {}, node, res = [], i;
    //hash map {id: key }
    for (i = 0; i < arr.length; i += 1) {
       map[arr[i].id] = i;
       arr[i].children = [];
    };
    for (i = 0; i < arr.length; i += 1) {
       node = arr[i];
       if (node.parentId !== "0") {
          arr[map[node.parentId]].children.push(node);
       }
       else {
          res.push(node);
       };
    };
    return res;
 };
 console.log(JSON.stringify(listToTree(arr), undefined, 4));

 const listToTree1 = (arr = []) => {
	let map = {}, res = [];
       //hash map {id: key }
	arr.forEach((element, i) => {
		map[element.id] = i;
	});

	arr.forEach((element) => {
		if (element.hasOwnProperty('parentId')) {
			arr[map[element.parentId]].children.push(element);
		} else {
			res.push(element);
		}
	});

	return res;
};
console.log(JSON.stringify(listToTree1(arr), undefined, 4));