/*// What are the different data types present in javascript?
A] Primitive types
    1. String
    2. Number
    3. Boolean
    4. Undefined
    5. Null

B] Non-primitive
    1. Object

*/

/*************************************************   assignment   ***************************************************** */
//  What is called Variable typing in Javascript?
// => Variable typing is used to assign a number to a variable. The same variable can be assigned to a string.
/*
var a = 10;
var a = "string;" // true

console.log(a); // string;

var b = 10;
let b = "string;" // Identifier 'b' has already been declared

console.log(b); // string;

let c = 10;
var c = "string;" // Identifier 'b' has already been declared

console.log(c); // string;
*/


/*************************************************  TYPE cONVERSION   ***************************************************** */

// ===> Implicite Conversion:=> where JavaScript Itself atomaticaly convert the type
// ===> Explicit Conversion:=> Where we manually convert the type


// Implicite Conversion
/*
console.log("1" - 1); //0
console.log(1 - "1"); //0
console.log(2 + "-2" + "2"); //2-22

console.log("Hello"+ "78");  // Hello78

console.log(true + '2'); // true2
console.log('2' + true); // 2true
console.log('2' - true); // -1
console.log('2' + false); // 2false
console.log('2' - false); // 2
console.log('2' - null); // 2  // null tritated as 0
console.log("4" - "2"); // 2
console.log("4" * "2"); // 8
console.log("4" / "2"); // 2
console.log("4" % "2"); // 0

console.log("Hello" + "World"); // HelloWorld
console.log("Hello" - "World"); // NaN
console.log("Hello" - "World" + 78); // NaN
*/

// Explicit Conversion
/*
console.log(Number('5')); // 5 (number)
console.log(Number('')); // 0 (number)
console.log(Number('true')); // NaN
console.log(Number(true)); // 1
console.log(Number('false')); // NaN
console.log(Number(false)); // 0

console.log(parseInt('5')); // 5
console.log(parseInt('3.14')); // 3

console.log(parseFloat('3.14')); // 3.14

console.log(String(500));  //500
console.log(String(true));  //true
console.log(String(null));  //null
console.log(String(undefinde)); //undefinde
console.log((500).toString());

console.log((null).toString()); // will not work
console.log((undefinde).toString()); // will not work

console.log(Boolean(10)); // true // null, undefined, 0, '', NaN will return false

*/

/*************************************************  Equality  ***************************************************** */

/*************************************************    ***************************************************** */

// console.log(a);
// a = 25;
// console.log(a);
// var a;


// greeting()      // greeting as function
// var greeting = function greeting() {
//     console.log("greeting as variable");
// }

// greeting()      // greeting as variable
// function greeting() {
//     console.log("greeting as function");
// }

// greeting()      // greeting as variable

/*
var variable = 10; // decelear globaly

(()=>{
    console.log("1",variable); // 10
    variable = 20; // <==== it is deceleared globaly
    console.log("2",variable); // 20
})()
console.log("3",variable); // 20
var variable = 30;

var variable = 10;

(()=>{
    console.log("4",variable);  // undefined
    var variable = 20; // // <==== it is deceleared functionaly
    console.log("5",variable); // 20
})()
console.log("6",variable); // 10
var variable = 30;

*/

/*
var variable = 10;
(()=>{
    variable_3 = 35;
    console.log(variable_3); // 35
    var variable_3 = 45;
    console.log(variable_3); // 45
    variable_2 = 15;
    console.log(variable); // 10
})();

console.log(variable_2); // 25
console.log(variable_3); // undefined
var variable = 100;
*/

////                                    \\\               Private counter
// let counter = function(){
//     let k = 0;
//     return () => k++;
// }();

// console.log(counter());
// console.log(counter());
// console.log(counter());


/*************************************************  Tricky Scope  ***************************************************** */
/*************************************************  Tricky Scope  ***************************************************** */
/*
// console.log("Start")
// let aaa = 222;

function name2() {
    let x = 2; 
    let y = 4;
    function name1() {
        console.log(x);
    }
    
    return name1;
}

function name3() {
    let x = 2; 
        let y = 4;
        function name1() {
            console.log(x);
        }
        
        return name1;
    }
    
    let fun = name2()
    let fun1 = name3()
    console.log(fun);
    fun()
    */
/*************************************************  Tricky Scope  ***************************************************** *
 
greeting();
var greeting = function (va) {
 console.log("var greeting");
}

greeting();

function greeting(fn) {
 console.log("function greeting");
}

greeting()
/*************************************************  Tricky Scope  ***************************************************** *
let count = function(){
 let k = 0
 return () => k++;
};

console.log(count()); 0
console.log(count()); 1
console.log(count()); 2
/*************************************************  Tricky Scope  ***************************************************** *
 function cerateCloserArray() {
 var badArr = [];

 // for (let index = 0; index < 5; index++) {
 for (var index = 0; index < 5; index++) {
     badArr[index] = function() {
         return 'n = ' + index;
     }
     
 }
 return badArr;
}

var badArr = cerateCloserArray();

for(var index in badArr){
 console.log(badArr[index]());
}
/*************************************************  Tricky Scope  ***************************************************** *
 var fullName = "T G";

var obj = {
    fullName: "TT GG",
    prop: {
        fullName: "PT PG",
        getFullName: function() {
            return this.fullName
        }
    },
    getMyName: function() {
        return this.fullName;
    },
    getFirstName: function() {
        return this.fullName.split(" ")[0];
    },
    getLastName: (function() {
        return this.fullName.split(" ")[1];
    })()

}

console.log(obj.prop.getFullName());
console.log(obj.getFirstName());
console.log(obj.getMyName());
console.log(obj.getLastName);
/*************************************************  Tricky Scope  ***************************************************** */
/*************************************************  Tricky Scope  ***************************************************** */

function debounce() {
    let timer;
    return function () {
        clearTimeout(timer);
        timer = setTimeout(()=>{
           console.log("interval");
        }, 1000)
    }
}

debounce()
