/*
function debounce(func, delay) {
    let timeoutId;
  
    return function (...args) {
      const context = this;
  
      clearTimeout(timeoutId);
      timeoutId = setTimeout(() => {
        func.apply(context, args);
      }, delay);
    };
  }
  
  // Example usage:
  const searchFunction = debounce((query) => {
    // Your search logic goes here
    console.log(`Searching for: ${query}`);
  }, 500);
  
  // Attach the debounced function to your search input
  const searchInput = document.getElementById('searchInput');
  searchInput.addEventListener('input', (event) => {
    searchFunction(event.target.value);
  });
  
*/

/*
let count = 0;

const getData = () =>{
    console.log("=====>", count++);
} 

const debounce = function () {
    let timer;

    return function () {

        clearTimeout(timer)
        timer = setTimeout(()=>{
            getData()
        }, 300)

    }
}

const betterFunction = debounce()
*/
