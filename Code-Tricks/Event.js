// By default it is event bubbling
document.getElementById("grandParent").addEventListener("click",()=>{
 console.log("grandParent");   
}, false)
document.getElementById("parent").addEventListener("click",(e)=>{
    e.stopPropogation();
 console.log("parent");   
}, false)
document.getElementById("child").addEventListener("click",()=>{
 console.log("child");   
}, false)

// // If true event capturing
document.getElementById("grandParent").addEventListener("click",()=>{
    console.log("grandParent");   
   }, true)
   document.getElementById("parent").addEventListener("click",()=>{
    console.log("parent");   
   }, true)
   document.getElementById("child").addEventListener("click",()=>{
    console.log("child");   
   }, true)

// // If true capturing happens and then bubbling
// document.getElementById("grandParent").addEventListener("click",()=>{
//     console.log("grandParent");   
//    }, true)
//    document.getElementById("parent").addEventListener("click",()=>{
//     console.log("parent");   
//    }, false)
//    document.getElementById("child").addEventListener("click",()=>{
//     console.log("child");   
//    }, true)

/*********************************************************
 *                       Event Delegation
 *********************************************************/
// In event delegation we add event lisener to the common parrent of the child and after clicking on child we capture it on parrent
document.querySelector("#category").addEventListener('click', (e)=>{
    console.log(e.target);
    console.log(e.target.id);
    if (e.target.tagName == 'LI') {
        console.log("li element only");
    }
});