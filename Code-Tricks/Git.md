# Version Control System (VCS) is a software that helps software developers to work together and maintain a complete history of their work

git --version

# To add chang user cread
git config --global user.name "Tushar
git config --global user.email "tushar@gmail.com"
#  You can change directly in config file
git config --global --edit



git init
# Add into staging area
git add {filename}  _OR_  git add .(it will add allall)
git commit -m 'Initial project version'
git push _OR_ git push origin {Branch name}

# To find all files
ls -a 
# Check the status of the file.
git status`

# To clone existing project
git clone {url}
# To Push local exesting project
git remote add origin {url}

git log

# to go to previous commit
git checkout {head of th ecommit}

# To go to branch
git checkout {branch name}

# To create new branch
git checkout -b {branch name}

# To check branch list
git branch

# to merge branch
git merge branch name
