/****************************************************************************************************** */
//                                               
/*
let arr = ["a", "b", "c"];

arr.newProp = "newVlue";

// key are the property keys
for (let a in arr) {
  console.log(a); // 0, 1, 2 & newValue
}

// value are the property values
for (let a of arr) {
  console.log(a); // a, b, c
}
/****************************************************************************************************** */
//                                               find() will gives reference to object
/*
users = [
    {fname:"Tushar", lname:"Ghayal", age: 26},
    {fname:"Prashant", lname:"Pathak", age: 26},
    {fname:"Vishal", lname:"Hulavle", age: 25},
    {fname:"Piyush", lname:"Barde", age: 28},
]

let a = users.find( u => u.fname === "Tushar")
a.fname = "ha ha"
console.log(a); // { fname: 'ha ha', lname: 'Ghayal', age: 26 }
console.log(users); // [
//     { fname: 'ha ha', lname: 'Ghayal', age: 26 },
//     { fname: 'Prashant', lname: 'Pathak', age: 26 },
//     { fname: 'Vishal', lname: 'Hulavle', age: 25 },
//     { fname: 'Piyush', lname: 'Barde', age: 28 }
//   ]


/****************************************************************************************************** */
//                                               Get duplicates Duplicate Count method 1
/*

const array = [1, 2, 2, 3, 3, 4, 5];
const duplicates = array.filter((value, index, self) => self.indexOf(value) !== index); //[2, 3] 



Using Map: If the array contains objects or if you need to track the occurrences of each value, 
you can use a Map to store the count of each value.

const array = [1, 2, 2, 3, 3, 4, 5];
const duplicates = [];
const countMap = new Map();

array.forEach(value => {
    countMap.set(value, (countMap.get(value) || 0) + 1);
    if (countMap.get(value) === 2) {
        duplicates.push(value);
    }
});


Using Set (Efficient): If performance is a concern and maintaining the order is not important, 
you can use a Set along with the has method to efficiently find duplicates.

const array = [1, 2, 2, 3, 3, 4, 5];
const duplicates = [];
const seen = new Set();

array.forEach(value => {
    if (seen.has(value)) {
        duplicates.push(value);
    } else {
        seen.add(value);
    }
});



/****************************************************************************************************** */
//                                               Duplicate Count method 1
/*
const names = ['Mike', 'Matt', 'Nancy', 'Adam', 'Jenny', 'Nancy', 'Carl']

const count = names =>
  names.reduce((a, b) => ({ ...a,
    [b]: (a[b] || 0) + 1
  }), {}) // don't forget to initialize the accumulator

const duplicates = dict => Object.keys(dict).filter((a) => dict[a] > 1)

console.log(count(names)) // { Mike: 1, Matt: 1, Nancy: 2, Adam: 1, Jenny: 1, Carl: 1 }
console.log(duplicates(count(names))) // [ 'Nancy' ]


//                                                Duplicate Count method 2

let d = names.reduce((acc, curr)=>{
    if (acc[curr]) {
        acc[curr] = ++acc[curr]
    }else{
        acc[curr] = 1;
    }
    return acc // don't forget return accumulator **
},{})// don't forget to initialize the accumulator **
console.log("===>",d);

let acc = { Mike: 1, Matt: 1, Nancy: 2, Adam: 1, Jenny: 1, Carl: 1 }
console.log(acc['Nancy']); // 2
console.log(acc.Nancy);  // 2

//                                                Duplicate Count method 3 with for

 let string = "aabbkdndiccoekdczufnrz".split('')

function abc(str){
   let = strin = str.reduce((acc, cur)=>{
    if (acc[cur]) {
        acc[cur] = acc[cur] + 1
    }else{
        acc[cur] = 1
    }
    return acc
   },{})
   return strin;
}

let dupli = abc(string)
console.log(dupli);


//                                                Duplicate Count method 3 with for
if (string) {
    let obj = {}
    for(let i=0; i< string.length; i++){
        if (obj[string[i]]) {
            obj[string[i]] += obj[string[i]]
        }else{
            obj[string[i]] = 1
        }
    }
    console.log(obj);
}



/****************************************************************************************************** */
/*

users = [
    {fname:"Tushar", lname:"Ghayal", age: 26},
    {fname:"Prashant", lname:"Pathak", age: 26},
    {fname:"Vishal", lname:"Hulavle", age: 25},
    {fname:"Piyush", lname:"Barde", age: 28},
]

let fullName = users.map( user => `${user.fname} ${user.lname}`)
console.log(fullName);

let sameAge = users.filter( user => user.age === 26)
console.log(sameAge);

fullNameSameAge = users.filter( user => user.age === 26).map( user => `${user.fname} ${user.lname}`)
console.log(fullNameSameAge);


let countSameAge = users.reduce((acc, curr)=>{
    if (acc[curr.age]) {
        acc[curr.age] = ++acc[curr.age];
    } else {
        acc[curr.age] = 1
    }
    
    return acc;
},{})


console.log(countSameAge);


const shots = [
    {id: 1, amount: 2},
    {id: 2, amount: 4},
    {id: 3, amount: 52},
    {id: 4, amount: 36},
    {id: 5, amount: 13},
    {id: 6, amount: 33}
  ];
  

//                                                Max Count method 1


//  let maxShots = shots.reduce((acc, shot) => acc = acc > shot.amount ? acc : shot.amount, 0);
let maxShots = shots.reduce((acc, curr)=> {
    if (acc > curr.amount) {
        return acc
    } else {                                   // Whatever we return that will get stored in the acc
        return curr.amount
    }
}, 0)
 console.log(maxShots);

//                                                Max Count method 2

                                                                                                                // //EXemple 1 : array of simple type
                                                                                                                // const array1 = [1,6,8,79,45,21,65,85,32,654];
                                                                                                                // const max1 = array1.reduce((op, item) => op = op > item ? op : item, 0);

                                                                                                                // //EXemple 1 : array of object 
                                                                                                                // const array2 = [ {id: 1, val: 60}, {id: 2, val: 2}, {id: 3, val: 89}, {id: 4, val: 78}];
                                                                                                                // const max2 = array2.reduce((op, item) => op = op > item.val ? op : item.val, 0);

                                                                                                                // console.log(max);
                                                                                                                // console.log(max2);



/******************************************************************************************************/




/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/








