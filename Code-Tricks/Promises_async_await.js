/*******************************************************
 *  Promises
 ***************************************************** */
/*
A promise is an object representing the eventual completion or failure of an asynchronous operation and its resulting value.
1. Before promise, we used to depend on callback functions which would result in 
    1.  Callback Hell (Pyramid of Doom) | 
    2. Inversion of control 
2. 2. Inversion of control is overcome by using promise.
        1. A promise is an object that represents the eventual completion/failure of an asynchronous operation. 
        2. A Promise has three states:
            1. Pending: The initial state when the Promise is created. The asynchronous operation is neither completed nor failed.
            2. Fulfilled: The operation was completed successfully, and the Promise has a resulting value.
            3. Rejected: The operation failed, and the Promise has a reason for the failure.
        3. As soon as the promise is fulfilled/rejected => It updates the empty object which is assigned undefined in the pending state. 
        4. A promise resolves only once and it is immutable. 
        5. Using .then() we can control when we call the cb(callback) function. 
3.  To avoid callback hell (Pyramid of Doom) => We use promise chaining. This way our code expands vertically instead of horizontally. Chaining is done using '.then()'
4. A very common mistake that developers make is not returning a value during the chaining of promises. Always remember to return a value. This returned value will be used by the next .then()
*/

/*

try: This statement is used to test a block of code for errors
catch: This statement is used to handle the error
throw: This statement is used to create custom errors.
finally: This statement is used to execute code after try and catch regardless of the result.

*/

/*******************************************************
 *  Creating Promises
 ******************************************************

function createOrder() {
    const promises = new Promise((resolve, rejected) => {
        const id = 22
        if (id > 10) {
            setTimeout(function () {
                resolve(id)
            }, 3000
            )
        } else {
            const msg = new Error("id not found")
            rejected(msg)
        }
    })

    return promises
}

const proceedToPayment = (id) =>{
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(`Payment successful`)
        }, 2000)
    })
}


//    *******************************************************
//              accepting Promises
//    ******************************************************

createOrder()
    .then((id) => {
        console.log("result",id);
        return id
    })
    .catch((err)=>{
        console.log("err",err.message);
    })
    .then((id)=>{
        return proceedToPayment(id) // when we return a promices from here
    })
    .then((result)=>{
        console.log(result); // this will work
    }).finally(()=>{
        console.log("Finally"); // This statement is used to execute code after try and catch regardless of the result.
    })
/*

    // with async await
const getOrder = async() => {
  try {
    const order = await createOrder();
    console.log(order);
    const payment = await proceedToPayment(order); 
    console.log(payment);
  } catch (err) {
    console.log(err); 
  }
}

getOrder();
*/
    
/*******************************************************
 *  async / await
 ******************************************************/
/*
// const p = Promise.resolve("Promise resolve")
// OR
const p1 = new Promise((resolve, reject)=>{
    setTimeout(() => {
        resolve("Promise resolve p1");
    }, 10000);
})

const p2 = new Promise((resolve, reject)=>{
    setTimeout(() => {
        resolve("Promise resolve p2");
    }, 4000);
})

const p3 = new Promise((resolve, reject)=>{
    setTimeout(() => {
        resolve("Promise resolve p3");
    }, 6000);
})

// before async/await

const resolvePromise = () =>{
    console.log("start");
    p1.then((res)=>{
        console.log(res);
    })
    console.log("middle 1");
    p2.then((res)=>{
        console.log(res);
    })
    console.log("middle 2");
    p3.then((res)=>{
        console.log(res);
    })
    console.log("end");
}

// // output
// start
// middle 1
// middle 2
// end
// Promise resolve p2
// Promise resolve p3
// Promise resolve p1

resolvePromise()

// // with async / await

const resolvePromise = async () =>{
    console.log("start");
    const res1 = await p1;
    console.log(res1);
    console.log("middle 1");
    const res2 = await p2;
    console.log(res2);
    console.log("middle 2");
    const res3 = await p3;
    console.log(res3);
    console.log("end");
}
resolvePromise()

*/

// error
// const fetchApi = () =>{
//     const data = fetch('https://jsonplaceholder.typicode.com/todos/1')
//       .then(response => response.json())
//       .then(json => console.log(json))
//       console.log("data", data);
// }

// fetchApi()

// -------------------------------------------------------------------------------------------
// const fetchApi = async () =>{
//     // await fetch('https://jsonplaceholder.typicode.com/todos')
//     //   .then(response => response.json())
//     //   .then(json => console.log(json))

//     ///////``````   OR 

//       const data = await fetch('https://jsonplaceholder.typicode.com/todos')
//       const data2 = await data.json()
//       console.log("data2", data2);
// }

// fetchApi()
// -------------------------------------------------------------------------------------------

// //                                                           error handeling
// const fetchApi = async () =>{
//     try {
//         const data = await fetch('https://jsonplaceholder.typicode.com/todos')
//         const data2 = await data.json()
//         console.log("data2", data2);
//     } catch (error) {
//         console.log(error);
//     }
// }

// fetchApi()
// -------------------------------------------------------------------------------------------
// //                                                           error handeling
// const fetchApi = async () =>{
//         const data = await fetch('https://jsonplaceholder.typicode.com/todos')
//       const data2 = await data.json()
//       console.log("data2", data2);
// }

// fetchApi().catch((error)=>{
//     console.log(error);
// })


// -----------------------------------------------------------------------------------------------------------
                                                    // Awaiting Non-Promise Values

// async function nonPromiseExample() {
//     return 42;
//   }
  
//   nonPromiseExample().then((result) => {
//     console.log(result); // Outputs: 42
//   });
  

/*******************************************************
 *  Promise.all 
 ******************************************************/
/*
// Takes an array of Promises and returns a new Promise that is fulfilled with an array of all resolved values 
// when all of the input Promises are fulfilled. 
// If any Promise is rejected, the returned Promise is immediately rejected.

const promise1 = Promise.resolve('One');
const promise2 = Promise.resolve('Two');
const promise3 = Promise.resolve('Three');

Promise.all([promise1, promise2, promise3])
  .then((values) => {
    console.log(values); // Outputs: ['One', 'Two', 'Three']
    //
    const [result1, result2, result3] = values;
    console.log(result1, result2, result3);
  })
  .catch((error) => {
    console.error(error);
  });

// ------------------------------------ OR

  async function parallelExecution() {
    const [result1, result2, result3] = await Promise.all([promise1, promise1, promise3]);
    console.log(result1, result2, result3);
  }
  parallelExecution()

/*******************************************************
 *  Promise.race
 ******************************************************/
/*
// return first resolved promise
// Takes an array of Promises and returns a new Promise that is settled
// Whatever will be the first output we get whether it is succes or faild return first

const fastPromise = new Promise((resolve) => setTimeout(() => resolve('Fast'), 100));
const slowPromise = new Promise((resolve) => setTimeout(() => resolve('Slow'), 500));

Promise.race([fastPromise, slowPromise])
  .then((winner) => {
    console.log(winner); // Outputs: 'Fast'
  })
  .catch((error) => {
    console.error(error);
  });

/*******************************************************
 *  Promise.any
 ******************************************************/
// return first any resolved or rejected promises
// if all fail throw aggrigated error
/*******************************************************
 *  Promise.allSettled
 ******************************************************/
// return all resolved and rejected promised