
                                        /************************************************************************
                                        /************************************************************************
                                                                    String Methods
                                        /***********************************************************************/
                                        /***********************************************************************/




/****************************************************************************************************** */
// //                                        Check palandrom
/*
var firstWord = "Maryy";
var secondWord = "Army";

if(firstWord.toLowerCase().split('').sort().join('') === secondWord.toLowerCase().split('').sort().join('')){
        console.log('True');
    
    }
*/
/*
    let str = "madam"
    function checkPalandrom(str) {
        for (let i = 1; i < str.split('').length / 2; i++) {
            if (str[i] !== str[str.length - 1 -i]) {
                 console.log("Not a palandrom");
                 return
                }
            }
            console.log("palandrom");
        return 
    }

    checkPalandrom(str)

*/
/****************************************************************************************************** */
////                                     Reverse string with word
/*

let str = 'Welcome to this Javascript'
let newStr = []
let arr = str.split(' ');

    arr.forEach(element => {
        let arr2 = element.split('');
        newStr.push(arr2.reverse().join('')); 
    });
    
    console.log(str);
    console.log(newStr.reverse().join(' '));




                                        /************************************************************************
                                        /************************************************************************
                                                                    Array Methods
                                        /***********************************************************************/
                                        /***********************************************************************/
// let array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

//                         Slice

// console.log(array.slice(5)); //[ 5, 6, 7, 8, 9 ]
// console.log(array.slice(0, 5)); // [ 0, 1, 2, 3, 4 ] did not include last element
// console.log(array.slice(5,8)); //  [ 5, 6, 7 ]
// console.log(array); //  No checnge in original array


//                         splice

// console.log(array.splice(5));  //[ 5, 6, 7, 8, 9 ]
// console.log(array);            //[ 0, 1, 2, 3, 4 ]

// console.log(array.splice(5,2));  //[5, 6]
// console.log(array);              //[0, 1, 2, 3, 4, 7, 8, 9]

// console.log(array.splice(5,4));  //[5, 6, 7, 8]
// console.log(array);              //[1, 2, 3, 4, 9]

// console.log(array.splice(5, 0));  //[  ]
// console.log(array.splice(5, 1));  //[ 5 ]
// console.log(array);               //[0, 1, 2, 3, 4, 6, 7, 8, 9]

//                                      add element to array

// console.log(array.splice(5, 0, 100));  //[  ]
// console.log(array);               //[ 0, 1, 2, 3, 4, 100, 5, 6, 7, 8, 9 ]


// console.log(array.splice(5, 2, 100));  //[ 5, 6 ]
// console.log(array);               //[ 0, 1, 2, 3, 4, 100, 7, 8, 9 ]


/************************************************************************
 How do you check whether an array includes a particular value or not

/***********************************************************************

var numericArray = [1, 2, 3, 4];
console.log(numericArray.includes(3)); // true

var stringArray = ['green', 'yellow', 'blue'];
console.log(stringArray.includes('blue')); //true



/************************************************************************
 How do you check whether an array is exact same or not 
/***********************************************************************
 const arrayFirst = [1,2,3,4,5];
 const arraySecond = [1,2,3,4,5];
 console.log(arrayFirst.length === arraySecond.length && arrayFirst.every((value, index) => value === arraySecond[index])); // true
 
 // If you would like to compare arrays irrespective of order then you should sort them before,
 
 console.log(arrayFirst.length === arraySecond.length && arrayFirst.sort().every((value, index) => value === arraySecond[index])); //true
 
 
 */

/***********************************************************************
 * Get unique from array
/***********************************************************************/
/*
    let arr = [ 2,3,5,2,4,6,3,5,53,56,35,5,3,455,3]
    let a = [...new Set(arr)]
    console.log(a);
 */
 
 /***********************************************************************
 *      unshift()     shift()                         push()      pop() 
 /**********************************************************************
  * The push() method adds one or more elements to the end of an array and returns the new length of the array.
  * The pop() method removes the last element from an array and returns that element.
  * 
  * The unshift() method adds one or more elements to the beginning of an array and returns the new length of the array.
  * The shift() method removes the first element from an array and returns that removed element.
 */
/*

 let arr = [1,2,3,4,5]

//  console.log(arr.push(6));    // add end
//  console.log(arr.pop());      // remove end
//  console.log(arr.unshift(6)); // add start
//  console.log(arr.shift());    // remove start


function rotateArray(arr, number) {
    for (let i = 1; i <= number; i++) {
        arr.unshift(arr.pop())
    }
    return arr;
}

let arr1 = rotateArray(arr, 2)
console.log(arr1);
*/

 /***********************************************************************
 * Write a function which takes an array and prints the majority element (if it exists), otherwise prints “No Majority Element”. 
 * A majority element in an array A[] of size n is an element that appears more than n/2 times (and hence there is at most one such element).
 /**********************************************************************/
/*

 let a = [ 1, 3, 4, 4, 4, 4, 3, 1, 2, 3, 4, 4, 4, 4 ];
    function majorityElement(arr){
        let obj = {}
        
        for (let i = 0; i < arr.length; i++) {
            if (obj[arr[i]]) {
                obj[arr[i]] += 1
            } else {
                obj[arr[i]] = 1
            }
        }
        return obj
    }

    let dup = majorityElement(a);

    function maxValu(dup, max) {
        for (const [key, value] of Object.entries(dup)) {
            if (value >= max) {
             console.log( key, value)
         }
        }
    }

    maxValu(dup, Math.round( a.length / 2))

/***********************************************************************
 *      Finding sum of digits of a number until sum becomes single digit
 /**********************************************************************/
 /*
//  Input : 1234
// Output : 1

let num = 1234;

let toString = String(num)
console.log(toString);

let toArrayString = toString.split("")
console.log(toArrayString);

let toArrayNumber= toArrayString.map((num)=>{
    return Number(num)
  });
console.log(toArrayNumber);


/***********************************************************************
 *      remove test field from array object // remove property from array
 /**********************************************************************/
 /*
 const arr = [
    {id: 1, name: 'Tom', test: 'abc'},
    {id: 2, name: 'Bob', test: 'xyz'},
  ];
  
  arr.forEach(object => {
    delete object['test'];
  });
  
  // 👇️ [{id: 1, name: 'Tom'}, {id: 2, name: 'Bob'}]
  console.log(arr);
//****************************************************************
const arr = [
    {id: 1, name: 'Tom', test: 'abc'},
    {id: 2, name: 'Bob', test: 'xyz'},
  ];
  
  const newArr = arr.map(({test, ...rest}) => {
    return rest;
  });
  
  // 👇️ [{id: 1, name: 'Tom'},  {id: 2, name: 'Bob'}]
  console.log(newArr);

/***********************************************************************
* What is the easiest way to convert an array to an object
/**********************************************************************
var fruits = ["banana", "apple", "orange", "watermelon"];
var fruitsObject = { ...fruits };
console.log(fruitsObject); // {0: "banana", 1: "apple", 2: "orange", 3: "watermelon"}


/***********************************************************************
* How do you map the array values without using map method
/**********************************************************************
const countries = [
    { name: "India", capital: "Delhi" },
    { name: "US", capital: "Washington" },
    { name: "Russia", capital: "Moscow" },
    { name: "Singapore", capital: "Singapore" },
    { name: "China", capital: "Beijing" },
    { name: "France", capital: "Paris" },
  ];
  
  const cityNames = Array.from(countries, ({ capital }) => capital);
  console.log(cityNames); // ['Delhi, 'Washington', 'Moscow', 'Singapore', 'Beijing', 'Paris']


/***********************************************************************
* How do you flattening multi dimensional arrays
/**********************************************************************

// Flattening bi-dimensional arrays is trivial with Spread operator.

const biDimensionalArr = [11, [22, 33], [44, 55], [66, 77], 88, 99];
const flattenArr = [].concat(...biDimensionalArr); // [11, 22, 33, 44, 55, 66, 77, 88, 99]
// But you can make it work with multi-dimensional arrays by recursive calls,

function flattenMultiArray(arr) {
  const flattened = [].concat(...arr);
  return flattened.some((item) => Array.isArray(item))
    ? flattenMultiArray(flattened)
    : flattened;
}
const multiDimensionalArr = [11, [22, 33], [44, [55, 66, [77, [88]], 99]]];
const flatArr = flattenMultiArray(multiDimensionalArr); // [11, 22, 33, 44, 55, 66, 77, 88, 99]
// Also you can use the flat method of Array.

const arr = [1, [2, 3], 4, 5, [6, 7]];
const fllattenArr = arr.flat(); // [1, 2, 3, 4, 5, 6, 7]

// And for multiDemensional arrays
const multiDimensionalArr = [11, [22, 33], [44, [55, 66, [77, [88]], 99]]];
const oneStepFlat = multiDimensionalArr.flat(1); // [11, 22, 33, 44, [55, 66, [77, [88]], 99]]
const towStep = multiDimensionalArr.flat(2); // [11, 22, 33, 44, 55, 66, [77, [88]], 99]
const fullyFlatArray = multiDimensionalArr.flat(Infinity); // [11, 22, 33, 44, 55, 66, 77, 88, 99]


var a = 100;
function createFunction() {
  var a = 200;
  return new Function("return a;");
}
console.log(createFunction()()); // 100
/***********************************************************************
* What is nullish coalescing operator (??)
/**********************************************************************
console.log(null ?? true); // true
console.log(false ?? true); // false
console.log(undefined ?? true); // true
console.log(0 ?? true); // true

let arr = [null, 4, false, 8, undefined, "", null, "Tushar", false, undefined, 0, 100]

console.log(arr.filter(Boolean));

/***********************************************************************
* 
/**********************************************************************/
/***********************************************************************
* 
/**********************************************************************/
/***********************************************************************
* 
/**********************************************************************/
/***********************************************************************
* 
/**********************************************************************/