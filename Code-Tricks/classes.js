// https://www.youtube.com/playlist?list=PLVvjrrRCBy2I2eE1e1yOuhts2oTvWesMy
/*
function constructureFunction(fname, age){
    this.fname = fname;
    this.age = age;

    this.info = function () {
        // console.log("==>",this); // return => constructureFunction
        return `${this.fname} ${this.age}`
    }
}

// create an object
let newFunction1 = new constructureFunction("Tushar1", 26)
// We are loging object create by constructor function
console.log(newFunction1);
// We are loging object.property create by constructor function
console.log(newFunction1.info());
let newFunction2 = new constructureFunction("Tushar2", 27)
console.log(newFunction2);
// In JavaScript, when this keyword is used in a constructor function, this refers to the object when the object is created. 
// Hence, when an object accesses the properties, it can directly access the property as
console.log(newFunction2.name);

//#  Create Objects: Constructor Function Vs Object Literal
// Object Literal is generally used to create a single object. The constructor function is useful if you want to create multiple objects.
// Each object created from the constructor function is unique. You can have the same properties as the constructor function or add a new property to one particular object.
newFunction2.lname = "Ghayal"
console.log(newFunction2);
//Output
// constructureFunction {
//     fname: 'Tushar2',
//     age: 27,
//     info: [Function (anonymous)],
//     lname: 'Ghayal'
//   }
// However, if an object is created with an object literal, and if a variable is defined with that object value, any changes in variable value will change the original object.

// You can also add properties and methods to a constructor function using a prototype.
constructureFunction.prototype.age1 = 99999
console.log(newFunction2); // cant see property in log
console.log(newFunction2.age1); // 99999

//# JavaScript Built-in Constructors

// let a = new Object();    // A new Object object
// let b = new String();    // A new String object
// let c = new Number();    // A new Number object
// let d = new Boolean();   // A new Boolean object

const name = new String ('John');
console.log(name); // "John"

const number = new Number (57);
console.log(number); // 57

// Note: It is recommended to use primitive data types and create them in a normal way, such as const name = 'John';, const number = 57; and const count = true;

/**********************************
 * JavaScript Classes // https://www.programiz.com/javascript/classes
 *********************************/
//  Classes are one of the features introduced in the ES6 version of JavaScript.
//  A class is a blueprint for the object. You can create an object from the class.
//  You can think of the class as a sketch (prototype) of a house. It contains all the details about the floors, doors, windows, etc. 
//  Based on these descriptions, you build the house. House is the object.
//  Since many houses can be made from the same description, we can create many objects from a class.

// JavaScript class is similar to the Javascript constructor function, and it is merely a syntactic sugar.

// // constructor function
/*
    function Person () {
        this.name = 'John',
        this.age = 23
    }

    // create an object
    const person1 = new Person();
*/

// Instead of using the function keyword, you use the class keyword for creating JS classes.
/*
// creating a class
class Person {
    constructor(name) {   // The constructor() method inside a class gets called automatically each time an object is created.
      this.name = name;
    }
  }
  
  // creating an object
  const person1 = new Person('John');
  const person2 = new Person('Jack');
  
  console.log(person1.name); // John
  console.log(person2.name); // Jack
*/
/*
// # Getters and Setters
// In JavaScript, getter methods get the value of an object and setter methods set the value of an object.
// JavaScript classes may include getters and setters. You use the get keyword for getter methods and set for setter methods. 

class Person {
    constructor(name) {
        this.name = name;
    }

    // getter
    get personName() {
        return this.name;
    }

    // setter
    set personName(x) {
        this.name = x;
    }
}

let person1 = new Person('Jack');
console.log(person1.name); // Jack

// changing the value of name property
person1.personName = 'Sarah';
console.log(person1.name); // Sarah
console.log(person1.personName); // Sarah
console.log(person1.personName()); // is not a function // Error because of getter method
*/

/* 
// # Hoisting
// A class should be defined before using it. Unlike functions and other JavaScript declarations, the class is not hoisted.
// accessing class

const p = new Person(); // ReferenceError
// defining class
class Person {
  constructor(name) {
    this.name = name;
  }
}
*/
/*
// # JavaScript Classes
// Classes always follow 'use-strict'. All the code inside the class is automatically in strict mode. For example,

class Person {
    constructor() {
      a = 0;
      this.name = a;
    }
  }
  
  let p = new Person(); // ReferenceError: Can't find variable: a

  /////// Note: JavaScript class is a special type of function. And the typeof operator returns function for a class.
  */

  /**********************************
 *Class Inheritance   https://www.programiz.com/javascript/inheritance
 *********************************/

//  Inheritance enables you to define a class that takes all the functionality from a parent class and allows you to add more.
// Using class inheritance, a class can inherit all the methods and properties of another class.
// Inheritance is a useful feature that allows code reusability.
// To use class inheritance, you use the extends keyword. For example,