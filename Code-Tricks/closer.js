/********************************************************************
 * Closer =>// Function along with lexical scope forms a closer
 * *****************************************************************/
/*
//check on console
function x() {
    let a = 20;
    function y(params) {
        console.log(a);
    }
    y()
}
x()
let aa = 200;
var bb = 2000;
*/
/*
function x() {
  let a = 20;
  var b = 30;
  function y() {
      console.log("let a",a); // let a 200
      console.log("var b",b); // var b 300
  }
  a = 200;
  b = 300;
  y()
}
x()
let aa = 200;
var bb = 2000;

*/
/*
let master = 10;
function outer() {
  let outer = 20;

  function middle() {
    let middle = 30;

    function inner() {
      console.log(master); // 10
      console.log(outer);  // 20
      console.log(middle); // 30
    }

    inner();

  }
  middle()
}
outer()
*/
/*


//                                     Private counter

function counter(){
  let count = 0;
  return function(){
    return  count = count +1
  }
}

let co = counter()
console.log(co());
console.log(co());


\\                                           Tricks
let aa = 10
function a(cc, dd){
  console.log(aa);
  console.log(cc);
  console.log(dd); // undefine
  function b(dd){
    console.log(aa);
    console.log(cc);
    console.log(dd);

  }

  return b
}

// a()(); 
// Or
let b = a(cc = "ccc")
console.log(b);
b(dd = "ddd")

/********************************************************************
 * Variable Environmen
 * *****************************************************************/
/*
 var x = 10;
 let y = 11;
 const z = 12;
 
 a();
 b();
 
 function a() {
   var x = 101;
   let y = 111;
   const z = 121;
   console.log(x, y, z);
 }
 
 function b() {
   var x = 102;
   let y = 112;
   const z = 122;
   console.log(x, y, z);
 }
 console.log(x);
 console.log(y);
 console.log(z);
 
 xyz = 123456
 console.log(xyz);

/********************************************************************
 * Variable Environmen
 * *****************************************************************/
/*
var x = 10;
let y = 11;
const z = 12;

a();
b();

function a() {
  var x = 101;
  let y = 111;
  const z = 121;
  console.log(x, y, z);
}

function b() {
  var x = 102;
  let y = 112;
  const z = 122;
  console.log(x, y, z);
}


/********************************************************************
 * Scope & Lexical Environment
 * *****************************************************************/
/*
function a(){
  console.log("Function a => ",outer_x, outer_y, outer_z); //Function a =>  10 11 12     // All will display because x, y, z and a() are in same scope
  
  function b() {
    console.log("Function b => ",outer_x, outer_y, outer_z); //Function b =>  10 11 12 // All will display because x, y, z and a() are in same scope
    function c() {
      console.log("Function c => ",outer_x, outer_y, outer_z); //Function c =>  10 11 12 // All will display because x, y, z and a() are in same scope
    }
    c()
  }

  b()
}

var outer_x = 10;
let outer_y = 11; 
const outer_z = 12;
a()


/********************************************************************
 * What are closures
 * *****************************************************************/


/********************************************************************
 * What is IIFE(Immediately Invoked Function Expression)
 * *****************************************************************/
/*
const user = (function() {
    let name = `anonymous`;
    return {
      getName: _ => name, //Or
    //   getName: () => name,
      setName: newName => name = newName
    };
  })();
  
  console.log(user.getName()) // anonymous
  user.setName(`Amy`);
  console.log(user.getName()); // Amy



  
  const user1 = (function() {
      let name = `anonymous`;
      return name;
    })();

    console.log(user1);

    */

/****************************************       CLOSURE    Counter      ************************************************************** */
/*
function createCount(){
    let x = 0;
    return function(){
      return x = ++x
    }
  }
  
  const count = createCount()
  console.log(createCount())
  console.log(count())
  console.log(count())
  console.log(createCount()())

// it will create new counter
const count1 = createCount()
console.log(count1())
console.log(count1())
*/
/*
// constructor function 
function counter() {
  let count = 0;
  this.increment = function () {
    count++;
    console.log(count);
  }
  this.decrement = function () {
    count--;
    console.log(count);
  }
}
// it is an constructor function so we have to use new keyword here
let counts = new counter()
counts.increment()
counts.increment()
counts.increment()
counts.decrement()

*/
/*

let ll = 1; // get store in script
const cc = 1; // get store in script
// a()
b()
c()

function a() {
    bb = 100;
    ll = 100;  // from script
    let kk = 20;
    let k1 = 20;
    // cc = 100;  // canot able to assign to constant
    console.log(ll);
    // console.log(ll1); // it is not create a closer with line 22 create closer with globle when it get call from function b
    // closer is depend on where the function get defined 
    function inner() {
        ff()
    }

    inner()

    function ff() {
        let bb = 20; // get store in local
        let b1 = 20; // get store in local
        k1 = 30;
        console.log(kk);
        console.log(ll);
        console.log(bb);
        console.log(b1);
        // console.log(ll1);
    }
    ff()



}

function b() {
    let ll = 200;
    let ll1 = 300;
    console.log(ll);
    a() // line 22
}

function c() {
    console.log(ll);
}

console.log(ll);
*/