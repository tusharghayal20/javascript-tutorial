/**********************************
 * Most console tricks work on Browser
 * *******************************
 */


/*
const foo = { name:"Tushar", age: 25,}
const bar = { name:"tush", age: 25,} 
const baz = { name:"trash", age: 25,}
console.log(foo); // you don't know the name in console
console.log({foo}); // you know the name in console
console.log({foo, bar, baz});
console.table([foo, bar, baz]);
//colour your text
console.log('%c Tushar', 'color: red; font-weight: bolder;'); // work in brouser
console.log("Multiple styles: %cred %corange", "color: red", "color: orange", "Additional unformatted message");
*/
/*
const deleteMe = () => console.trace('bbbbb') // works on browser // give the infonmation where this function get called and how many times
deleteMe()
*/

/*
for (let index = 0; index < 5; index++) {
    // console.count(); // method logs the number of times that this particular call to
    // console.count([index]);
    console.count("Tushar");
    console.countReset("Tushar"); // reset count
}
*/
/*
console.error("Display as error in log") // work in brouser
*/
/*
//You can use nested groups to help organize your output by visually associating related messages. 
console.log("This is the outer level");
console.group();
console.log("Level 2");
console.group();
console.log("Level 3");
console.warn("More of level 3");
console.groupEnd();
console.log("Back to level 2");
console.groupEnd();
console.log("Back to the outer level");
*/
/*
console.time("answer time");
for (let index = 0; index < 10000; index++) {
    console.timeLog("answer time");
}
console.timeEnd("answer time");
*/