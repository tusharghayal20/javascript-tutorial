/********************************************************************
 * All
 * *****************************************************************/
/*
//Function Statement //Function Decleration

function name(){             //
  console.log("name");       //  This is function statement
}                            //

//Function expression

let fun = function name(){
  console.log("name");
}
// Or
let fun = function (){
  console.log("name");
}

//Anonymous function
//are a function wher we can use as an value
function (){
  console.log("");
}

                                                                //Name function Expression
let fun = function xyz(){
  console.log("xyz");
  //we can acces this function here
  console.log(xyz); //true
}

// call
fun() //true
xyz() //false

                                                                // Diff Parameters and Arguments

function name(param1, param2){
  console.log("xyz");
} 

name(arg1, arg2)

                                                                // What are lambda or arrow functions?

// => An arrow function is a shorter syntax for a function expression and does not have its own this, 
// arguments, super, or new.target. These functions are best suited for non-method functions, 
// and they cannot be used as constructors.


                                                                  //What is a first class function?
// => First-class functions => variable
// means when functions in that language are treated like any other variable 
//a function can be passed as an argument to other functions, 
// can be returned by another function and can be assigned as a value to a variable



//What is a first order function?
// => First-order function is a function that doesn’t accept another function as an argument 
// and doesn’t return a function as its return value.


// What is a higher order function?
// => Higher-order function is a function that accepts another function as an argument 
//or returns a function as a return value or both.


// What is a unary function?
// => Unary function (i.e. monadic) is a function that accepts exactly one argument. 
// It stands for a single argument accepted by a function.


// What is the currying function?
// => currying is the process of taking a function with multiple arguments and turning it 
// into a sequence of functions each with only a single argument. 

// const multiArgFunction = (a, b, c) => a + b + c;
// console.log(multiArgFunction(1,2,3));// 6

// const curryUnaryFunction = a => b => c => a + b + c;
// curryUnaryFunction (1); // returns a function: b => c =>  1 + b + c
// curryUnaryFunction (1) (2); // returns a function: c => 3 + c
// curryUnaryFunction (1) (2) (3); // returns the number 6


// What is a pure function?
// => A Pure function is a function where the return value is only determined by its arguments without any side effects. 
// i.e, If you call a function with the same arguments 'n' number of times and 'n' number of places in the application then 
// it will always return the same value


/********************************************************************
 * What is IIFE(Immediately Invoked Function Expression)
 * *****************************************************************/
/*
const user = (function() {
    let name = `anonymous`;
    return {
      getName: _ => name, //Or
    //   getName: () => name,
      setName: newName => name = newName
    };
  })();
  
  console.log(user.getName()) // anonymous
  user.setName(`Amy`);
  console.log(user.getName()); // Amy
  
  const user1 = (function() {
      let name = `anonymous`;
      return name;
    })();

    console.log(user1);



*/

/******************************************************************************************************/
/*
const circle1 = {
  radius: 1,
  location:{
      x:1,
      y:1
  },
  draw: function (params) {
      console.log("Draw circle1");
  }
}

circle1.draw()

//TO avoid multiple time writing this code we can create Factory code
//If it return obj then it is Factory function
function FactoryCircle(radius) {
  return{
      radius: radius,
      draw: function (params) {
          console.log("Draw FactoryCircle");
      }
  }
}

const factorycircle = FactoryCircle(1)
console.log(factorycircle);
factorycircle.draw()

//Constructor function
//If we use this keyword along with new operator then it is Constructor function
function ConstructorCircle(radius) {
  this.radius = radius;
  this.draw = function () {
      console.log("Draw ConstructorCircle");
  }

  // we don't have to return anything thing 
  //because when we use ne it wit do it automaticaly
}

const Constructor_Circle = new ConstructorCircle(1)
console.log(Constructor_Circle);
Constructor_Circle.draw()
*/
/******************************************************************************************************/
/*
const a = function(){
  console.log(this); //global/window object

  const b = {
    func1: function(){
      console.log(this); // object "b"
    }  
  }

  const c = {
    func2: ()=>{                                       //Since we are using arrow function inside func2, this keyword refers to the global object.
      console.log(this); // global/window object
    }
  }

  b.func1();
  c.func2();
}

a();
*/
/******************************************************************************************************/
/*

const b = {
  name:"Vivek",
  f: function(){
    var self = this;
    console.log(this.name);   /// "Vivek"
    (function(){
      console.log(this.name);  // undefined
      console.log(self.name); /// "Vivek"
    })();
  }
}

b.f();
// Only in the IIFE inside the function f , the this keyword refers to the global/window object.
*/
/******************************************************************************************************/
/*
(function(a){
  return (function(){
    console.log(a);// 45 // Even though a is defined in the outer function, due to closure the inner functions have access to it.
    a = 23;
  })()
})(45);
*/

/*
// Each time bigFunc is called, an array of size 700 is being created,
// Modify the code so that we don't create the same array again and again

function bigFunc(element){
  let newArray = new Array(700).fill('♥');
  return newArray[element];
}


// This code can be modified by using closures,
function bigFunc(){
  let newArray = new Array(700).fill('♥');
  return (element) => newArray[element];
}


let getElement = bigFunc(); // Array is created only once
getElement(599);
getElement(670);  
*/
/******************************************************************************************************/
/*
// The following code outputs 2 and 2 after waiting for one second
// Modify the code to output 0 and 1 after one second.

function randomFunc(){
  for(var i = 0; i < 2; i++){
    setTimeout(()=> console.log(i),1000);
  }
}

randomFunc();


//                                                                  Using let keyword:

function randomFunc(){
  for(let i = 0; i < 2; i++){
    setTimeout(()=> console.log(i),1000);
  }
}

randomFunc(); 

//                                                                    Using closure:

function randomFunc(){
  for(var i = 0; i < 2; i++){
  (function(i){
      setTimeout(()=>console.log(i),1000);
    })(i);
  }
}

randomFunc();  
*/

/***************************************************************** Compose    *********** *

const compose = (...functions) =>{
  return (...arg) =>{
      return functions.reduceRight((arg, fun)=>{
          console.log(arg);
          return fun(arg)
      }, ...arg)
  }
}


const add5 = (x) => x + 5;
const multiplyBy2 = (x) => x * 2;
const subtract10 = (x) => x - 10;

const composedFunction = compose(subtract10, multiplyBy2, add5);

console.log(composedFunction(3)); // Output: ((3 + 5) * 2) - 10 = 6

