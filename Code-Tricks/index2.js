/************************   ****************************************************************************** */
/************************   ****************************************************************************** */

// function lexicalScope(params) {
//     console.log(a);  
//     console.log(b); 
//     console.log(c); 
// }

// lexicalScope()
// //a undefine
// //b undefine
// //c undefine

// var a = 10;
// let b = 20;
// const c = 30;


/************************ BLOCK SCOPE ****************************************************************************** */
// var is an functional sope 
// let, const are block scope

// Each blok has it's lexical scope
/*
{
    var a = 10;
    let b = 20;
    const c = 30;

    console.log(a); // 10 
    console.log(b); // 20
    console.log(c); // 30
}
console.log(a); // 10
console.log(b); // b is not defined
console.log(c); // c is not defined

*/

/*
// //                                                                                      shadowing
var shadowing = 100;  // this is n global scope
let b = 100;                 // this b is in Script scope
{
    var shadowing = 10;
    let b = 10;             // this b is in block scope // No error
                                   console.log("shadowing",shadowing); // 10 
            console.log("b",b); // 10  <=== dif
}
                                   console.log("shadowing",shadowing); // 10
            console.log("b",b); // 100  <=== dif

*/

/*

var a = 20;
let aa = 20;
const aaa = 20;
{
    var a = 30; // possible
    let aa = 30; // possible
    const aaa = 30; // possible
}
console.log("a",a); // 30
console.log("aa",aa); // 20
console.log("aaa",aaa); // 20

var a = 100;
let b = 100;
const c = 100;
//{} Block is different from function block  // this act as different
{
    var a = 200;
    let b = 200;
    const c = 200;
    console.log("block a", a);  //200
    console.log("block a this", this.a);    //200
    console.log("block b", b);  //200
    console.log("block b this", this.b);    //undefine  // we don't have value of let in global scope it is in separate space
    console.log("block c", c);  // 200
    console.log("block c this", this.c);    //undefine  // we don't have value of const in global scope it is in separate space
}
console.log("out a", a);    //200 //var value get overwritten for global as var get declared in global space
console.log("out b", b);    //100
console.log("out c", c);    //100
function abc() {
    var a = 300;    // this function create separate execution context on top of global execution context it can fetch value from global from this but can't add value on global as it is initialise in functional block
    let b = 300;
    const c = 300;
    console.log("fun block a", a);  //300
    console.log("fun block a this", this.a);    //200
    console.log("fun block b", b);  //300
    console.log("fun block b this", this.b);    //undefine
    console.log("fun block c", c);  //300
    console.log("fun block c this", this.c);    //undefine
}
console.log("before fun call a", a);    //200
console.log("before fun call a this", this.a);  //200
console.log("before fun call b", b);    //100
console.log("before fun call b this", this.b);  //undefine
console.log("before fun call c", c);    //100
console.log("before fun call c this", this.c);  //undefine
abc()
console.log("after fun call a", a); //200
console.log("after fun call a this", this.a);   //200
console.log("after fun call b", b); //100
console.log("after fun call b this", this.b);   //undefine
console.log("after fun call c", c); //100
console.log("after fun call c this", this.c);   //undefine
function xyz() {
    //lexical concept get in use
    console.log("fun block a", a);  //200
    console.log("fun block a this", this.a);    //200
    console.log("fun block b", b);  //100
    console.log("fun block b this", this.b);    //undefine
    console.log("fun block c", c);  //100
    console.log("fun block c this", this.c);    //undefine
}
xyz()
console.log("after fun call a", a); //200
console.log("after fun call a this", this.a);   //200
console.log("after fun call b", b); //100
console.log("after fun call b this", this.b);   //undefine
console.log("after fun call c", c); //100
console.log("after fun call c this", this.c);   //undefine

*/
/*


//  let b = 20;
//  {
//      var b = 20; // Identifier 'b' has already been declared
//  }

 var c = 20;
 {
     let c = 20;  // possible
 }
console.log("c",c);
// We can assign value multiple times but canot decleare it again and again

//=============================

let a = 10;
var a = 20; // SyntaxError: Identifier 'a' has already been declared
console.log(a);



*/
/*

var a = 20;
var a = 30; // workx
// let a = 40; //  SyntaxError: Identifier 'a' has already been declared

function b() {
    let a = 50;      // It will works because of scope
    console.log(a); // 50
}

b()

console.log(a); // 30


/*
    var a; // OR let a; // same output   
    console.log(a); // undefine
    a = 10;
    console.log(a); // 10
    a = "Tushar"
    console.log(a); // Tushar
/* const a will throw an error Because const need to assign at declerstion*/

/*
    function a() {
        console.log(aa);
    }
    a() // undefine
    var aa = 10;
    a() // 10
*/

/*
    function a() {
        function b() {
            console.log(aa); 
        }
        b()
    }
    a() // undefine
    var aa = 10;
    a() // 10
*/

/*
function a() {
    var aa = 10;
    function b() {
        var bb = 10;
    }
    b()
}
console.log(aa); // aa is not defined
console.log(bb); // bb is not defined

function a() {
        console.log(aa);     // 10
    function b() {
        console.log(bb);     // 20
    }
    b()
}
var aa = 10;
var bb = 20;
a()
*/

/*
function x() {
  let a = 20; 
  var b = 30;
  function y() {
      console.log("let a",a); // let a 200
      console.log("var b",b); // var b 300
  }
  a = 200;
  b = 300;
  y()
}
x()
let aa = 200;
var bb = 2000;

*/



/****************************************************************************************************** */
//                                                     Trick
/*
let t = 100;

function test() {
    return setTimeout(() => {
        console.log(t); // 100
        t = t * 2;
        console.log(t); // 200
    }, 5000);
}

let res = test() + t; //return is 1 so 1 + 100 = 101
console.log(res); // 101

/****************************************************************************************************** */
//                                                     Trick
/*

// The Temporal Dead Zone is a behavior in JavaScript that occurs when declaring a variable with the let and const keywords, but not with var. 
function somemethod() {
  console.log(counter1); // undefined
  console.log(counter2); // ReferenceError
  var counter1 = 1;
  let counter2 = 2;
}




let a;
console.log("a",a); // undefine
a = 10;
console.log("a",a);  //10


// console.log("let b",b);  // ReferenceError: Cannot access 'b' before initialization
console.log("var bb",bb);   // undefined
let b;
var bb;

console.log("let b", b); // undefined
console.log("var bb", bb); // undefined

b = 10;
bb = 1010;

console.log("b",b); // 10
console.log("bb",bb); // 1010


var c;
console.log("c",c); // undefine
// console.log("d",d);  // ReferenceError: d is not defined

/****************************************************************************************************** */
//                                                     Trick
/*

let a = 0;
let b = false;
console.log((a == b)); //true
console.log((a === b)); // false






if (0 == false) {
    console.log("True"); //<=
}else{
    console.log("Nothing");
}

if ('0' == false) {
    console.log("True"); //<=
}else{
    console.log("Nothing");
}

if (0 === false) {
    console.log("Nothing");
}else{
    console.log("false"); //<=
}

if ('0' === false) {
    console.log("Nothing");
}else{
    console.log("false"); //<=
}

/****************************************************************************************************** */
//                                                     Trick
/*
var a=3;
var b=a++;
var c=++a;
console.log(a,b,c)
// console.log(5,3,5)
/****************************************************************************************************** */
//                                                     Trick
/*
const number = undefined + 11;
console.log(number); // NaN

if (number === NaN || number == NaN) {
 console.log("NaN");
} else if (number === 11 || number == 11) {
 console.log("11");
} else {
 console.log("other");  //<=========== In both case only execute it
}

//Output
// Other

/****************************************************************************************************** */
//                                                     Trick
/*
//with array
const a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

   //let  <===============
for (let i = 0; i < 10; i++) {
  setTimeout(() => console.log(a[i]), 1000);
}
Output
1
2
3
4
5
6
7
8
9
10

    //var  <====
for (var i = 0; i < 10; i++) {
  setTimeout(() => console.log(a[i]), 1000);
}

//Output
undefined
undefined
undefined
undefined
undefined
undefined
undefined
undefined
undefined
undefined

    //var  <====
for (var i = 0; i < 10; i++) { //i
    setTimeout(() => console.log(i), 1000);
  }

//Output // last index i++ value after completing loop is 10 not 9  <======
  10
  10
  10
  10
  10
  10
  10
  10
  10
  10


  //without array
// let  <====
for (let i = 0; i < 10; i++) {
    setTimeout(() => console.log(i), 1000);
}
    
//Output
0
1
2
3
4
5
6
7
8
9

*/

/*
function timeOut() {
    for (var index = 0; index < 10; index++) {
        setTimeout(()=>{
            console.log(index);
        },3000)
    }
}
timeOut()
// After 3 sec it will print 10 10 times


function timeOut2() {
    for (let index = 0; index < 10; index++) {
        setTimeout(()=>{
            console.log(index);
        },index * 3000) //<========
    }
}
timeOut2()
// it will print 0 t0 9 one by on aftereach second
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9


function timeOut3() {
    for (var index = 0; index < 10; index++) {
        function close(x) {
            setTimeout(()=>{
                console.log(x);
            }, x * 3000)
        }
        close(index)
    }
}
timeOut3()
// it will print 0 t0 9 one by on aftereach second
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
*/

/****************************************************************************************************** */
/****************************************************************************************************** */
//                                                 comma operator
/*

var x = 1;
x = (x++, x);

console.log(x); // 2

for (var a = 0, b =10; a <= 10; a++, b--); //<=====  look at comma at the end of for it will stop for loop

function myFunction() {
    var a = 1;
    return (a += 10, a); // 11 // return last value of a 
 }

 console.log(myFunction())
 

/****************************************************************************************************** */
//                               precedence order between local and global variables
/*
var msg = "Good morning";
function greeting() {
   msg = "Good Evening";
   console.log(msg);  // Good Evening
}
console.log(msg); // Good morning
greeting(); /// 

// Output
// Good morning
// Good Evening

/****************************************************************************************************** */
//                                                     Trick
/*
console.log(1+2+'3'); //==> 33
console.log(1+2-'3'); //==> 0 

/****************************************************************************************************** */
//                                                     Trick


//a.js
/*
var a = 10;
console.log(a); // 10
console.log(b); // undefine
// console.log(c); // not define
let aa = 30;



// b.js
var b = 20;
console.log(a); // 10
console.log(b); // 20
console.log(aa); // 30


// Undefined: It occurs when a variable has been declared but has not been assigned with any value. Undefined is not a keyword.

// Undeclared: It occurs when we try to access any variable that is not initialized or declared earlier using var or const keyword. 
//  If we use ‘typeof’ operator to get the value of an undeclared variable, we will face the runtime error with return value as “undefined”. 
//  The scope of the undeclared variables is always global.
*/


/********************************************************************************************************* */
/*
var x = 23;

(function(){
  var x = 43;
  (function random(){
    x++;
    console.log(x);  // NaN 
    var x = 21;
  })();
})();

// Output is NaN .

// random() function has functional scope, since x is declared and hoisted in the functional scope.
// Rewriting the random function will give a better idea about the output:

function random(){
    var x; // x is hoisted
    x++; // x is not a number since it is not initialized yet
    console.log(x); // Outputs NaN
    x = 21; // Initialization of x
  }

/********************************************************************************************************* */
/*************************************     get unique from array        *********************************************** */
// let arr = [ 2,3,5,2,4,6,3,5,53,56,35,5,3,455,3]

// let a = [...new Set(arr)]
// console.log(a);
