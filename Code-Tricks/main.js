// // https://www.toptal.com/javascript/interview-questions


/* ===============================================  1  ========================================================== *
// differencr between fun, obj, arr

let fun = function name(params) {
    console.log(a);
}

let obj = {
    "a": "b"
}

let arr = [1, 2, 3, 4, 5, 6, 7, ]

console.log(typeof fun); // function 
console.log(typeof obj); // object
console.log(typeof arr); // object

console.log(fun.length); // 1
console.log(obj.length); // undefine
console.log(arr.length); // 7

console.log(fun.name); // name
console.log(obj.name); // undefine
console.log(arr.name); // undefine

console.log(fun.toString());    // ƒ name(params) {
                                //     console.log(a);
                                // }
console.log(obj.toString()); // [object Object]
console.log(arr.toString()); // 1,2,3,4,5,6,7

console.log(fun.valueOf()); // ƒ name(params) {
                            //     console.log(a);
                            // }
console.log(obj.valueOf()); // [object Object]
console.log(arr.valueOf()); // [1,2,3,4,5,6,7]
*/

// (function(){
//     var a = b = 3;
//   })();

//   console.log("a defined? " + (typeof a !== 'undefined')); // false 
//   console.log("b defined? " + (typeof b!== 'undefined'));  // true




/* ===============================================  5  ========================================================== *
"use strict";
b = 3;

/* ===============================================  5  ========================================================== *

function sum(a) {
    return function (b) {
        return a + b
    }
}

console.log(sum(1,2));
console.log(sum(1)(2));

/* ===============================================  11  ========================================================== *
for (var i = 0; i < 5; i++) {
    var btn = document.createElement('button');
    btn.appendChild(document.createTextNode('Button ' + i));
    btn.addEventListener('click', function(){ console.log(i); });
    document.body.appendChild(btn);
  }
/* ===============================================  12  ========================================================== *
var d = {};
[ 'zebra', 'horse' ].forEach(function(k) {
    d[k] = undefined;
});

console.log(d);
/* ===============================================  13  ========================================================== *
var arr1 = "john".split('');
var arr2 = arr1.reverse();
//
// var arr2 = [...arr1.reverse()];
var arr3 = "jones".split('');
console.log(arr1); // ['n', 'h', 'o', 'j']
console.log(arr2); // ['n', 'h', 'o', 'j']
console.log(arr3); // ['j', 'o', 'n', 'e', 's']

arr2.push(arr3);
console.log("after");

console.log(arr1); // ['n', 'h', 'o', 'j', Array(5)]
console.log(arr2); // ['n', 'h', 'o', 'j', Array(5)]
console.log(arr3); // ['j', 'o', 'n', 'e', 's']


// Q
console.log("array 1: length=" + arr1.length + " last=" + arr1.slice(-1));  // slice give new array
console.log("array 2: length=" + arr2.length + " last=" + arr2.slice(-1));
/* ===============================================  14  ========================================================== *
// plus convert to int
console.log(1 +  "2" + "2");    //  122
console.log(1 +  +"2" + "2");   //  32
console.log(1 +  -"1" + "2");   //  02
console.log(+"3" + -"2");   //  1
console.log("3" + -"2");   //  3-2
console.log(+"1" +  "1" + "2");   //  112
console.log( "A" - "B" + "2");   //  NaN2
console.log( "A" - "B" + 2);   //  NaN
/* ===============================================  17  ========================================================== *

console.log("0 || 1 = "+(0 || 1));
console.log("1 || 2 = "+(1 || 2));
console.log("0 && 1 = "+(0 && 1));
console.log("1 && 0 = "+(1 && 0));
console.log("1 && 2 = "+(1 && 2));
console.log("if boolean value come first it show boolean or other case last value if first is true");
console.log("2 && 1 = "+(2 && 1));
console.log("2 && 3 = "+(2 && 3));
console.log("3 && 2 = "+(3 && 2));

/* ===============================================  5  ========================================================== *
console.log((function f(n){
    console.log(n);
    return ((n > 1) ? n * f(n-1) : n)
})(10));

// Output
// f(1): returns n, which is 1
// f(2): returns 2 * f(1), which is 2
// f(3): returns 3 * f(2), which is 6
// f(4): returns 4 * f(3), which is 24
// f(5): returns 5 * f(4), which is 120
// f(6): returns 6 * f(5), which is 720
// f(7): returns 7 * f(6), which is 5040
// f(8): returns 8 * f(7), which is 40320
// f(9): returns 9 * f(8), which is 362880
// f(10): returns 10 * f(9), which is 3628800

/* ===============================================  26  ========================================================== *
var hero = {
    _name: 'John Doe',
    getSecretIdentity: function (){
        return this._name;  
    }
};

let stoleSecretIdentity = hero.getSecretIdentity();

console.log(stoleSecretIdentity);
console.log(hero.getSecretIdentity());
/* ===============================================  27  ========================================================== *
var x = 21;
girl = function () {
    console.log(x);
    var x = 20;
};
girl ();

// Neither 21, nor 20, the result is undefined
/* ===============================================  5  ========================================================== *
for (let i = 0; i < 5; i++) {
    setTimeout(function() { console.log(i); }, i * 1000 );
  }
/* ===============================================  Object map  ========================================================== *
let user = {
    name: "John",
    age: 30
  };

  // list of kee
console.log(Object.keys(user)) // ["name", "age"]   

  //list of value
console.log(Object.values(user)) // ["John", 30]

  list of key value pair
console.log(Object.entries(user)) // [ ["name","John"], ["age",30] ]

/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */
/* ===============================================  0  ========================================================== */

// const array = [1, 2, 2, 3, 3, 4, 5];
// // const unique = [...new Set(array)];
// const duplicates1 = array.filter((value, index, self) => self.indexOf(value) !== index)
// const duplicates = [...new Set(array.filter((value, index, self) => self.indexOf(value) !== index))];
// console.log("duplicates", duplicates1);
// console.log("duplicates", duplicates);
/*
const pro = new Promise((resolve, reject)=>{
    setTimeout(() => {
        resolve("Hisb kshd ks")
    }, 1000);
    
})


console.log("1");

setTimeout(() => {
    console.log("10000");
}, 5000);

console.log("2");

setTimeout(() => {
    console.log("0000");
}, 0);

console.log("3");

pro.then(res=> {
    console.log(res);
})

*/
/*
(function name() {
    console.log(a);
    console.log("hi");
})(20)
*/
/*
let a = [1,2,3,4]

a.shift()
a.push(5)
a.unshift(1)
a[4] = 5
console.log(a);
*/
/*
function manup(arr) {
    arr.push(5)
    arr = [1]
    return arr
}

let list = [1,2,3,4]
manup(list)
console.log(list);

list = manup(list)
console.log(list);
*/

/*
let sample = new Set();
sample.add("Hello");
sample.add(1)
sample.add("Bye")
sample.add("@");

for (let item of sample) {
    console.log(item);
}

let sample1 = new Map();
sample1.set("name", "Ram");
sample1.set("Role", "SDE")
sample1.set("Country", "India")

for (let item of sample1) {
    console.log(item);
}
*/

// const object = { a: 1, b: 2, c: 3 };
// for (const property in object) {
//   console.log(`${property}: ${object[property]}`);
// }
// Object.keys(user) = ["name", "age"] .map()
// Object.values(user) = ["John", 30]
// Object.entries(user) = [ ["name","John"], ["age",30] ]

/*
let person = {
    firstName: 'John',
    lastName: 'Doe'
};

function greet() {
    console.log(this.firstName);
}

person.greet = greet;

person.greet();

*/
/*
var name = "Tushar"
const obj = {
    name: 'John',
    sayName: function () {
        console.log(this.name); // 'John'       
    },
    sayNameArrow: () => {
        console.log(this.name); // undefined (arrow functions don't have their own 'this')
    }
};

obj.sayName()   // 'John'
obj.sayNameArrow() // 'Tushar

*/

// function solution(A) {
//     // Implement your solution here
//     let a = [...new Set(A)]
//     for(let i = 1; i <= 100000; i++){
//       if (!a.includes(i)) {
//         return i
//       }
//     }
// }

// console.log(solution([1, 3, 6, 4, 1, 2]));

// function solution(A) {
//     const positiveSet = new Set();

//     // Add positive integers to the set
//     for (let i = 0; i < A.length; i++) {
//         if (A[i] > 0) {
//             positiveSet.add(A[i]);
//         }
//     }
//     console.log(positiveSet);
//     // Find the smallest positive integer not in the set
//     for (let i = 1; i <= 100000; i++) {
//         if (!positiveSet.has(i)) {
//             return i;
//         }
//     }

//     // If all positive integers up to 100000 are present, return 100001
//     return 100001;
// }

// // Test cases
// console.log(solution([3, 6, 4, 8])); // Output: 5
// console.log(solution([1, 2, 3]));          // Output: 4
// console.log(solution([-1, -3]));          // Output: 1
// let board = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// let s = '';
// for (let i = 0, j = 1; i < board.length; i++, j++) {
//   s += board[i] + ' ';
//   if (j % 3 == 0) {
//     console.log(s);
//     s = '';
//   }
// }


/*
//                                          Merge 2 sorted array
let arr1 = [1,2,6,7,9]
let arr2 = [3,4,5,8,10]
function mergeSortedArrays(a, b) {
    const mergedArray = [];
    let i = 0; // Pointer for array a
    let j = 0; // Pointer for array b

    // Merge arrays a and b into mergedArray in sorted order
    while (i < a.length && j < b.length) {
        if (a[i] <= b[j]) {
            mergedArray.push(a[i]);
            i++;
        } else {
            mergedArray.push(b[j]);
            j++;
        }
    }

    // Add remaining elements from array a, if any
    while (i < a.length) {
        mergedArray.push(a[i]);
        i++;
    }

    // Add remaining elements from array b, if any
    while (j < b.length) {
        mergedArray.push(b[j]);
        j++;
    }

    return mergedArray;
}

const a = [2, 4, 6, 8];
const b = [3, 5, 7, 9];
const mergedArray = mergeSortedArrays(a, b);

console.log("Merged sorted array:", mergedArray); // Output: Merged sorted array: [2, 3, 4, 5, 6, 7, 8, 9]
*/

/*
const testObj = {
  name: "John",
  age: 10,
  address: {
    street: "St street",
    code: {
      name: "first",
      pin: "1234"
    }
  }
};

function getDeft(testObj) {
  let maxDeft = 1;
  function name1(params) {
    console.log("params", params);
    for (const key in params) {
      if (typeof(params[key]) == 'object' && params[key] !== null) {
        name1(params[key])
        maxDeft++
      }
    }
    
  }
  name1(testObj)

  console.log(maxDeft);
}

getDeft(testObj)
*/



const str = "aaswesasxdesawsxzsaqwedcxswqaz";
const uniqueChars = [...new Set(str)];

console.log(uniqueChars);



