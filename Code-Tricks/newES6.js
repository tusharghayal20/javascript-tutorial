// https://www.programiz.com/javascript/ES6
//      . let / const   
//      . Arrow function
//      . Classes
//      . Rest parameter / Spread operator
//      . Object/Array Destructuring
//      . Template Literals
//      . Default parameters              function(name, age = 20)
//      . Import and export
//      . 
//      . 
//      . 
//      . 
//      . 



/**********************************
 * Arrow Function  https://www.programiz.com/javascript/arrow-function
 *********************************/
/*
//  Arrow Function Syntax
 let myFunction = (arg1, arg2, ...argN) => {
    statement(s)
}

let myFunction = (arg1, arg2, ...argN) => expression

let greet = () => console.log('Hello');
greet(); // Hello


// Arrow Function as an Expression
let age = 5;

let welcome = (age < 18) ?
  () => console.log('Baby') :
  () => console.log('Adult');

welcome(); // Baby

// this with Arrow Function
function Person() {
    this.name = 'Jack',
    this.age = 25,
    this.sayName = function () {

        // this is accessible
        console.log(this.age); // 25

        function innerFunc() {

            // this refers to the global object
            console.log(this.age); // undefine
            console.log(this); // Window {}
        }

        innerFunc();

    }
}

let x = new Person();
x.sayName();



function Person() {
    this.name = 'Jack',
    this.age = 25,
    this.sayName = function () {

        console.log(this.age); // 25
        let innerFunc = () => {
            console.log(this.age); // 25
        }

        innerFunc();
    }
}

const x = new Person();
x.sayName();

// Arguments Binding 
// Regular functions have arguments binding. 
// That's why when you pass arguments to a regular function, you can access them using the arguments keyword. For example,

let x = function () {
    console.log(arguments);
}
x(4,6,7); // Arguments [4, 6, 7]

let x = () => {
    console.log(arguments);
}

x(4,6,7); // ReferenceError: Can't find variable: arguments
// To solve this issue, you can use the spread syntax. For example,
let x = (...n) => {
    console.log(n);
}

x(4,6,7); // [4, 6, 7]

// Arrow Function with Promises and Callbacks
// ES5
asyncFunction().then(function() {
    return asyncFunction1();
}).then(function() {
    return asyncFunction2();
}).then(function() {
    finish;
});

// ES6
asyncFunction()
.then(() => asyncFunction1())
.then(() => asyncFunction2())
.then(() => finish);

// Things You Should Avoid With Arrow Functions
// 1] You should not use arrow functions to create methods inside objects.
let person = {
    name: 'Jack',
    age: 25,
    sayName: () => {

        // this refers to the global .....
        //
        console.log(this.age);
    }
}
// 2] You cannot use an arrow function as a constructor.
let Foo = () => {};
let foo = new Foo(); // TypeError: Foo is not a constructor

person.sayName(); // undefined

/**********************************
 * Rest parameter / Spread operator
 *********************************/
/*
 const arrValue = ['My', 'name', 'is', 'Jack'];
 console.log(arrValue);   // ["My", "name", "is", "Jack"]
 console.log(...arrValue); // My name is Jack


//  Copy Array Using Spread Operator
const arr1 = ['one', 'two'];
const arr2 = [...arr1, 'three', 'four', 'five'];
console.log(arr2); //  ["one", "two", "three", "four", "five"]


// Clone Array Using Spread Operator
let arr1 = [ 1, 2, 3];


// copy using spread syntax
let arr2 = [...arr1];
console.log(arr1); // [1, 2, 3]
console.log(arr2); // [1, 2, 3]
arr1.push(4);
console.log(arr1); // [1, 2, 3, 4]
console.log(arr2); // [1, 2, 3]


// Spread Operator with Object
const obj1 = { x : 1, y : 2 };
const obj2 = { z : 3 };
const obj3 = {...obj1, ...obj2};
console.log(obj3); // {x: 1, y: 2, z: 3}

// // // // Rest Parameter
let func = function(...args) {
    console.log(args);
}
func(3); // [3]
func(4, 5, 6); // [4, 5, 6]


function sum(x, y ,z) {
    console.log(x + y + z);
}
const num1 = [1, 3, 4, 5];
sum(...num1); // 8

/**********************************
 * JavaScript Classes // https://www.programiz.com/javascript/classes
 *********************************/
//  Classes are one of the features introduced in the ES6 version of JavaScript.
//  A class is a blueprint for the object. You can create an object from the class.
//  You can think of the class as a sketch (prototype) of a house. It contains all the details about the floors, doors, windows, etc. 
//  Based on these descriptions, you build the house. House is the object.
//  Since many houses can be made from the same description, we can create many objects from a class.

// JavaScript class is similar to the Javascript constructor function, and it is merely a syntactic sugar.

// // constructor function
/*
    function Person () {
        this.name = 'John',
        this.age = 23
    }

    // create an object
    const person1 = new Person();
*/

// Instead of using the function keyword, you use the class keyword for creating JS classes.
/*
// creating a class
class Person {
    constructor(name) {   // The constructor() method inside a class gets called automatically each time an object is created.
      this.name = name;
    }
  }
  
  // creating an object
  const person1 = new Person('John');
  const person2 = new Person('Jack');
  
  console.log(person1.name); // John
  console.log(person2.name); // Jack
*/
/*
// # Getters and Setters
// In JavaScript, getter methods get the value of an object and setter methods set the value of an object.
// JavaScript classes may include getters and setters. You use the get keyword for getter methods and set for setter methods. 

class Person {
    constructor(name) {
        this.name = name;
    }

    // getter
    get personName() {
        return this.name;
    }

    // setter
    set personName(x) {
        this.name = x;
    }
}

let person1 = new Person('Jack');
console.log(person1.name); // Jack

// changing the value of name property
person1.personName = 'Sarah';
console.log(person1.name); // Sarah
console.log(person1.personName); // Sarah
console.log(person1.personName()); // is not a function // Error because of getter method
*/

/* 
// # Hoisting
// A class should be defined before using it. Unlike functions and other JavaScript declarations, the class is not hoisted.
// accessing class

const p = new Person(); // ReferenceError
// defining class
class Person {
  constructor(name) {
    this.name = name;
  }
}
*/
/*
// # JavaScript Classes
// Classes always follow 'use-strict'. All the code inside the class is automatically in strict mode. For example,

class Person {
    constructor() {
      a = 0;
      this.name = a;
    }
  }
  
  let p = new Person(); // ReferenceError: Can't find variable: a

  /////// Note: JavaScript class is a special type of function. And the typeof operator returns function for a class.
  */