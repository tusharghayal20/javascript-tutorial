/********************************************************************
 * Obj Decleration and access value
 * *****************************************************************

 let obj = {
  name: 'Tushar',
  "full name": 'Tushar Ghayal'
}

console.log(obj);
console.log(obj.name);
console.log(obj["full name"]);

/********************************************************************
 * Call Applay Bind
 * *****************************************************************/
/*
**************** EX1
let obj = {
    name: "Tushar",
    lname: "Ghayal"
}

let fullName = function (age, city, text){
    console.log(this.name + " " + this.lname + " age is "+ age+ ", " + city + " " + text)
}

// Call: The call() method invokes a function with a given this value and arguments provided one by one
// execute the current function immediately.
fullName.call(obj, "30", "Pune", "Call method")

// Apply: Invokes the function with a given this value and allows you to pass in arguments as an array
// execute the current function immediately.
fullName.apply(obj, ["30", "Pune", "applay method"])

// Bind: returns a new function, allowing you to pass any number of arguments
// Bind creates a new function
let newFunction = fullName.bind(obj, "30", "Pune", "Bind method")
newFunction()


// Polyfill for bind method
// creating own bind method


Function.prototype.myBind = function(...args){ // args = obj, "30", "Pune"
    console.log(this, args);
  let fullNameFunction = this, //in 'this' we get function on which we bind
  params = args.slice(1)

  return function(...args2){ // args2 = "Polyfill bind method" // anything passed to the function call
    fullNameFunction.apply(args[0], [...params, ...args2]) 
  }
}

// OR

Function.prototype.myBind = function(...args) { 
    return (params) => this.apply(args[0], [...args.slice(1), params]); 
}

let newFunction2 = fullName.myBind(obj, "30", "Pune")
newFunction2("Polyfill bind method")

*/
/*

************ EX2


const obj = {
	fname: "Tushar",
	lname: "Ghayal",
	fullname: function(city , state ) {
		return `${this.fname} ${this.lname} ${city} ${state}`
	}
}

function fullname2 (city , state ) {
	return `${this.fname} ${this.lname} ${city} ${state}`
}

const obj2 = {
	fname: "Raj",
	lname: "Warma",
}

let call = obj.fullname.call(obj2, 'Pune', 'Maharashtra')
console.log(call);
            // seperet function
let call2 = fullname2.call(obj2, 'Pune', 'Maharashtra')
console.log(call2);

const array = ['Pune', 'Maharashtra'];
// let applay = obj.fullname.apply(obj2, ['Pune', 'Maharashtra'])
let applay = obj.fullname.apply(obj2, array)
console.log(applay);


let bind = obj.fullname.bind(obj2, 'Pune', 'Maharashtra') 
console.log(bind());  // It is same as call but need to call as function


//********************************************************************
 const person = {
     fname:"Tushar",
     lname:"Ghayal",
     fullname: function () {
         return `${this.fname} ${this.lname}`
     }
 }

let fullname = person.fullname
console.log(fullname()); /// undefined undefined
let person2 = {
    fname:"Tus",
     lname:"Gha"
}

let f = fullname.apply(person2)
console.log(f); // Tus Gha



/********************************************************************
 * list of keys of any object
 * *****************************************************************/
/*

const newObject = {
    a: 1,
    b: 2,
    c: 3,
  };
  console.log(Object.keys(newObject))   //["a", "b", "c"];
  console.log(Object.values(newObject))   //["1", "2", "3"];
  console.log(Object.entries(newObject))  // [["a"][1],["b"][2],["c"][3]]
  console.log(Object.getOwnPropertyNames(newObject));   //["a", "b", "c"];
  
*/

/*
const user = {
  name: 'John',
  gender: 'male',
  age: 40
};

console.log(Object.keys(user)); //['name', 'gender', 'age']
console.log(Object.values(user)); // [ 'John', 'male', 40 ]

console.log(Object.entries(user)); // [ [ 'name', 'John' ], [ 'gender', 'male' ], [ 'age', 40 ] ]
					// OR
for (let [key, value] of Object.entries(user)) {
    console.log(`${key}: ${value}`); 	// name: John
										// gender: male
										// age: 40
  }

let check = user.hasOwnProperty("name")
console.log(check);
console.log(user.name !== undefined); // true
/********************************************************************
 * Check value and object
 * *****************************************************************/
/*
 const objectEntries = {
    a: 'Good morning',
    b: 100
  };
  
  for (let [key, value] of Object.entries(objectEntries)) {
    console.log(`${key}: ${value}`); // a: 'Good morning'
                                     // b: 100
  }

  const objectValue = {
    a: 'Good morning',
    b: 100
  };
 
  for (let value of Object.values(objectValue)) {
    console.log(`${value}`); // 'Good morning'
                                 100
  }
 */

/********************************************************************
 * loop object
 * *****************************************************************/
/*
 var object = {
  "k1": "value1",
  "k2": "value2",
  "k3": "value3"
};

for (var key in object) {
  if (object.hasOwnProperty(key)) {
      console.log(key + " -> " + object[key]); // k1 -> value1 ...
  }
}



/********************************************************************
 * copy properties from one object to other / we can clone object
 * *****************************************************************/
/*
 const target = { a: 1, b: 2 };
 const source = { b: 3, c: 4 };
 
 const returnedTarget = Object.assign(target, source);

 console.log(target); // { a: 1, b: 3, c: 4 }

 console.log(returnedTarget); // { a: 1, b: 3, c: 4 }


/********************************************************************
 * Change Object without mutation   //https://youtu.be/poQXNp9ItL4
 * *****************************************************************/

// Both of this method create shallow copy
/*Method 1*/
/*
const person = {
  name: 'Tushar',
	address: { 
    city: 'Pune', 
    pin: 411011 
  },
};
//Copy object to another

const CopyedPerson = Object.assign({}, person); //Person object get copied into {} obj
console.log('CopyedPerson', CopyedPerson);

const updatedPerson1 = Object.assign({}, person, { name: 'Tush', age: 26 });

console.log('person', person);
console.log('updatedPerson1', updatedPerson1);

/*Method 2*/
/*
const person = { name: 'Tushar', address: { city: 'Pune', pin: 411011 } };
const updatedPerson = { ...person, name: 'Tush', age: 26 };
console.log('person', person);
console.log('updatedPerson', updatedPerson);

/**************************************** */
/*
// Both of this method create shallow copy
// so if we created
updatedPerson.address;
console.log('address', updatedPerson.address);
updatedPerson.address.city = 'Mumbai';
console.log('Updated address', updatedPerson.address);
// But it will also change person object because in shallow copy address object still pointing toward the person.address
console.log('person address', person.address, 'changed');

/**************************************** */
/*
// to solve this we have to create deep copy
const updatedPerson1 = {
  ...person,
	address: { ...person.address, city: 'Mumbaiiiii' },
	name: 'Tush',
	age: 26,
};
console.log('Updated1 address', updatedPerson1.address);
console.log('person address', person.address, 'not changed'); // not changed
*/


/********************************************************************
 *Q:- How to convert an Object {} into an Array [] in JavaScript?
 * *****************************************************************/
/*
 let obj = { id: "1", name: "Test User", age: "25", profession: "Developer" };

 console.log(Object.keys(obj));  // [ 'id', 'name', 'age', 'profession' ]

 console.log(Object.values(obj));  // [ '1', 'Test User', '25', 'Developer' ]
 
 console.log(Object.entries(obj));  // [ [ 'id', '1' ], [ 'name', 'Test User' ], [ 'age', '25' ], [ 'profession', 'Developer' ] ]

/********************************************************************
 *Q:- How to convert an Array []  into an Object {}  in JavaScript?
 * *****************************************************************/
/*
let arr = ["1", "Test User", "25", "Developer"];
let arr1 = [
  ["id", "1"],
  ["name", "Test User"],
  ["age", "25"],
  ["profession", "Developer"],
];

//Method 1: Using Object.assign() method
console.log(Object.assign({}, arr));
//{0: "1", 1: "Test User", 2: "25", 3: "Developer"}

// Method 2 Using Spread operator
console.log({ ...arr });
//{0: "1", 1: "Test User", 2: "25", 3: "Developer"}

// Method 3: Using Object.fromEntries() method
console.log(Object.fromEntries(arr1));
//{id: "1", name: "Test User", age: "25", profession: "Developer"}




var fruits = ["banana", "apple", "orange", "watermelon"];
var fruitsObject = {...fruits};
console.log(fruitsObject); // {0: "banana", 1: "apple", 2: "orange", 3: "watermelon"}
*/

/********************************************************************
 *Q:- How do you add a key value pair in javascript
 * *****************************************************************/
/*
 var object = {
  key1: value1,
  key2: value2
};

object.key3 = "value3";
obj["key3"] = "value3";

console.log(object);



/********************************************************************
 *Q:- Freeze / Seal
 * *****************************************************************/
/*
 const objConst = {
  fname:"Tushar",
  lname: "Ghayal"
} 

const objFreeze = {
  fname:"Tushar",
  lname: "Ghayal"
} 

const objSeal = {
  fname:"Tushar",
  lname: "Ghayal"
} 
// const Obj              // we can add/change property and value
Object.seal(objSeal)      // we canot add property but can change value 
Object.freeze(objFreeze)  // we canot do anything

objConst.fname= ""
objSeal.fname= ""
objFreeze.fname= ""

objConst.fullName= "Tushar Gahyal"
objSeal.fullName= "Tushar Gahyal"
objFreeze.fullName= "Tushar Gahyal"


console.log("objConst",objConst);
console.log("objSeal",objSeal);
console.log("objFreeze",objFreeze);



/********************************************************************
 *Q:- 
 * *****************************************************************/
/********************************************************************
 *Q:- 
 * *****************************************************************/
/********************************************************************
 *Q:- 
 * *****************************************************************/
/********************************************************************
 *Q:- 
 * *****************************************************************/