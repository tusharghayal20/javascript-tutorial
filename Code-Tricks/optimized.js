/*******************************************************
 *  “The fastest code is the code that never runs.”
 ***************************************************** */ 
/*******************************************************
 ***********  Avoid Unnecessary Steps
 ***************************************************** */ 

/*
//running some code is a lot slower than running no code!
console.log('incorrect'.split('').slice(2).join(''));
console.log('incorrect'.slice(2));



/*******************************************************
 ***********  Break Out of Loops As Early As Possible
 ***************************************************** */ 

/*
 let haystack = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
 let needle = 6;

//  for (let i = 0; i < haystack.length; i++) {
//      console.log(i);
//     if (haystack[i] === needle){
//         console.log(" haystack === needle");
//     }
//   }

//  for (let i = 0; i < haystack.length; i++) {
//      console.log(i);
//     if (haystack[i] === needle){
//         console.log(" haystack === needle");
//         break
//     }
// }

 for (let i = 1; i < haystack.length; i++) {
     if (haystack[i] === needle){
        console.log(i);
        console.log(" haystack === needle"); // if true then continue
        continue;
        testContinue() // not printed
    }
    if (haystack[i] === 8){
        console.log(i);
        console.log(" haystack === needle continue");
        testContinue()
    }
}

function testContinue() {
    console.log("testContinue");
}

//It’s also worth remembering that it’s possible to break out of nested loops using labels. 
//These allow you to associate a break or continue statement with a specific loop:

loop1: for (let i = 0; i < haystacks.length; i++) {
  loop2: for (let j = 0; j < haystacks[i].length; j++) {
    if (haystacks[i][j] === needle) {
      break loop1;
    }
  }
}



/*******************************************************
 ***********Pre-Compute Once Wherever Possible
 ***************************************************** */ 

/*
 function whichSideOfTheForce(name) {
    const light = ['Luke', 'Obi-Wan', 'Yoda']; 
    const dark = ['Vader', 'Palpatine'];
    
    return light.includes(name) ? 'light' : 
      dark.includes(name) ? 'dark' : 'unknown';
  };
  whichSideOfTheForce('Yoda');   // returns "light"
  whichSideOfTheForce('Anakin'); // returns "unknown"

//  The problem with this code is that every time we call whichSideOfTheForce , we create a new object. 
//  With every function call, memory is unnecessarily re-allocated to our light and dark arrays.

function whichSideOfTheForce2(name) {
    const light = ['Luke', 'Obi-Wan', 'Yoda'];
    const dark = ['Vader', 'Palpatine'];

    return name => light.includes(name) ? 'light' :
      dark.includes(name) ? 'dark' : 'unknown';
};
whichSideOfTheForce2('Yoda');   // returns "light"
whichSideOfTheForce2('Anakin'); // returns "unknown"


function doSomething(arg1, arg2) {
    function doSomethingElse(arg) {
      return process(arg);
    };
    return doSomethingElse(arg1) + doSomethingElse(arg2);
  }
  //Every time we run doSomething , the nested function doSomethingElse is created from scratch. Again, closures provide a solution. 
  //If we return a function, doSomethingElse remains private but it will only be created once:

  function doSomething(arg1, arg2) {
    function doSomethingElse(arg) {
      return process(arg);
    };
    return (arg1, arg2) => doSomethingElse(arg1) + doSomethingElse(arg2);
  }

  //Insatate of combine variable return variables combins function


/*******************************************************
 ***********  
 ***************************************************** */ 
/*
let array = []
for (let index = 0; index < 10000; index++) {
    array.push(index)
}

console.time('map')
let map = array.map(element => element * 2) // 11.5ms
// console.log("map",map);
console.timeEnd('map')

console.time('for')
let forArray = []
for (let index = 0; index < array.length; index++) {  // 
    const element = array[index];
    forArray.push(element * 2)
    
}
// console.log("forArray",forArray);
console.timeEnd('for')

console.time('forEach')
let forEachh =  []
array.forEach(element => {
    forEachh.push(element * 2);
});
// console.log("forEach",forEachh);
console.timeEnd('forEach')


/*******************************************************
 ***********
 ***************************************************** */ 
/*******************************************************
 ***********
 ***************************************************** */ 
/*******************************************************
 ***********
 ***************************************************** */ 
/*******************************************************
 ***********
 ***************************************************** */ 