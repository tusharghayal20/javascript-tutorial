// // 1. If with multiple conditions
// // We can store multiple values in the array and we can use the array includes method.

// let x = 'abc'
// //longhand
// if (x === 'abc' || x === 'def' || x === 'ghi' || x ==='jkl') {
//     console.log(x);
// }

// //shorthand
// let arr = ['abc', 'def', 'ghi', 'jkl']
// if (arr.includes(x)) {
//    console.log(x);
// }

/********************************************************* */

// // 2. If true … else Shorthand
// // This is a greater short cut for when we have if-else conditions that do not contain bigger logics inside. We can simply use the ternary operators to achieve this shorthand.
// // Longhand
// let x = 150;
// if (x > 100) {
//     test1 = true;
// } else {
//     test1 = false;
// }
// console.log(test1);

// // Shorthand
// let test2 = (x > 10) ? true : false;
// console.log(test2);
// //or we can use directly
// let test3 = x > 10;
// console.log(test3);


// // When we have nested conditions we can go this way.
// let x = 300,
// test4 = (x > 100) ? 'greater 100' : (x < 50) ? 'less 50' : 'between 50 and 100';
// console.log(test4); // "greater than 100"

/********************************************************* */
// // Longhand
// if (test1 === true)

// // Shorthand
// if (test1)
/********************************************************* */

//    //Longhand 
//    if (test1) {
//     callMethod(); 
//    } 
//    //Shorthand 
//    test1 && callMethod();
/********************************************************* */

// // foreach Loop Shorthand
// // This is one of the common shorthand technique for iteration.
// // Longhand
// let testData = [11, 12, 13, 14, 15]
// for (var i = 0; i < testData.length; i++){
//     console.log(testData[i]);
// }

// // Shorthand
// for (let i in testData){
//     console.log(testData[i]);
// }  

// for (let i of testData){
//     console.log(i);
// }


// // Array for each variable
// function testDataFunction(element, index, array) {
//   console.log('test[' + index + '] = ' + element);
// }
// testData.forEach(testDataFunction);
// // // logs: test[0] = 11, test[1] = 12, test[2] = 13 ....

/********************************************************* */
// Short Function Calling
// We can use the ternary operator to achieve these functions.

// // Longhand
// function test1() {
//     console.log('test1');
// };
// function test2() {
//     console.log('test2');
// };

// var test3 = 1;
// if (test3 == 1) {
//     test1();
// } else {
//     test2();
// }
// // Shorthand
// test3 === 1? test1():test2();
//         //OR
// (test3 === 1? test1:test2)();

/********************************************************* */
// Switch Shorthands
// We can save the conditions in the key-value objects and can be used based on the conditions.

// function test1() {
// console.log('test1');
// };
// function test2() {
// console.log('test2');
// };
// function test3() {
// console.log('test3');
// };
// // Longhand
// data = 1
// switch (data) {
//     case 1:
//       test1();
//     break;
  
//     case 2:
//       test2();
//     break;
  
//     case 3:
//       test();
//     break;
//     // And so on...
// }


//   // Shorthand
//   var data = {
//     1: test1,
//     2: test2,
//     3: test3,
//   };
  
//   console.log(data[1]);
// // data[1] && data[1]();

/********************************************************* */
// //Spread Operator Shorthand
// //longhand
// // joining arrays using concat
// const data = [1, 2, 3];
// const test = [4 ,5 , 6].concat(data);
// //shorthand
// // joining arrays
// const data = [1, 2, 3];
// const test = [4 ,5 , 6, ...data];
// console.log(test); // [ 4, 5, 6, 1, 2, 3]

/********************************************************* */

// const data = [{
//     type: 'test1',
//     name: 'abc'
// },
// {
//     type: 'test2',
//     name: 'cde'
// },
// {
//     type: 'test1',
//     name: 'fgh'
// },
// ]
// function findtest1(name) {
// for (let i = 0; i < data.length; ++i) {
//     if (data[i].type === 'test1' && data[i].name === name) {
//         return data[i];
//     }
// }
// }
// //Shorthand
// filteredData = data.find(data => data.type === 'test1');
// console.log(filteredData); // { type: 'test1', name: 'fgh' }

/********************************************************* */
/*
// Bitwise IndexOf Shorthand
// When we are iterating an array to find a specific value we do use indexOf() method.What if we find a better approach for that? Let’s check out the example.
let arr = [1, 2, 3, 4, 5, 6, 7, 8]
//longhand
let item = 9;
if(arr.indexOf(item) > -1) { // item found 
    console.log("item found",item);
}
if(arr.indexOf(item) === -1) { // item not found
    console.log("item not found",item);
}

//shorthand
if(~arr.indexOf(item)) { // item found
    console.log("item found",item);
}
if(!~arr.indexOf(item)) { // item not found
    console.log("item not found",item);
}
// The bitwise(~) the operator will return a truthy value for anything but -1. Negating it is as simple as doing !~. Alternatively, we can also use the includes() function:
if (arr.includes(item)) { 
    console.log("item found",item);
// true if the item found
}
/********************************************************* */


/******************************************************************************
*                             Avoid Loop
    https://levelup.gitconnected.com/4-things-you-have-to-unlearn-to-become-a-better-programmer-547adf476445
/******************************************************************************/
/*
const groceries = [
    {
      name: 'Face Masks',
      price: 17.50,
    },
    {
      name: 'Disinfecting Wipes',
      price: 24.99,
    },
    {
      name: 'Goggles',
      price: 8.99,
    },
    {
      name: 'Gloves',
      price: 25.99,
    },
    {
      name: 'Hand Sanitizers',
      price: 24.99,
    },
  ];

  let index = 0;
while (index < groceries.length) {
  console.log(groceries[index].name);
  index = index + 1;
}

// Face Masks
// Disinfecting Wipes
// Goggles
// Gloves
// Hand Sanitizers


// Instate

groceries.forEach((item) => {
    console.log(item.name);
  });

// Face Masks
// Disinfecting Wipes
// Goggles
// Gloves
// Hand Sanitizers
*/

/*
//to create array of price
const groceries = [
    {
      name: 'Face Masks',
      price: 17.50,
    },
    {
      name: 'Disinfecting Wipes',
      price: 24.99,
    },
    {
      name: 'Goggles',
      price: 8.99,
    },
    {
      name: 'Gloves',
      price: 25.99,
    },
    {
      name: 'Hand Sanitizers',
      price: 24.99,
    },
  ];



let index = 0;
const prices = [];
while (index < groceries.length) {
  prices.push(groceries[index].price);
  index = index + 1;
}
console.log(prices);
//[ 17.5, 24.99, 8.99, 25.99, 24.99 ]

// Instate


let pricesMap = groceries.map((item) => {
  return item.price;
});
console.log(pricesMap);
//[ 17.5, 24.99, 8.99, 25.99, 24.99 ]
*/


/*
// To add price
const groceries = [
    {
      name: 'Face Masks',
      price: 17.50,
    },
    {
      name: 'Disinfecting Wipes',
      price: 24.99,
    },
    {
      name: 'Goggles',
      price: 8.99,
    },
    {
      name: 'Gloves',
      price: 25.99,
    },
    {
      name: 'Hand Sanitizers',
      price: 24.99,
    },
  ];



let index = 0;
let total = 0;
while (index < groceries.length) {
  total = total + groceries[index].price;
  index = index + 1;
}
console.log(total);

// Instate

let totalReduce = groceries.reduce((sum, item) => {
  return sum += item.price;
}, 0);
console.log(totalReduce);



/********************************************************* */
/********************************************************* */
/********************************************************* */
/********************************************************* */