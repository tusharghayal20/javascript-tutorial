"use strict";
/**
 * with strict mode this keyword behave differently 
 */

let a = this;
let b = this;
let c;

function x() {
    c = this;
    console.log(c); // with strict mode it is undefine else window obj
}
x()

console.log("1", a === b); // true
console.log("2", a === c); // if we use strict mode then false else true

/*
 * this substitution
*  only in non strict mode if the value of this is undefine or null it will get replaced by global obj
 */ 

/**
 * this keyword value depends on how we call a function
 */
console.log("3");
x() // undefine
console.log("4");
window.x() // global obj
console.log("5");
x.apply(this) // global obj

/**
 * this inside a obj
 */
let obj = {
    name: "Tushar",
    printName: function() {
        console.log(this.name);
    },
    x: function () {
        console.log(this); // point to this obj
    },
    y: ()=>{
        console.log(this); // this points to it's enclosing lexicsl obj
    }
}

console.log("6");

obj.x() // point to this obj
obj.y() // this points to it's enclosing lexicsl obj



console.log("7");

let obj2 = {
    name: "Tushar Ghayal"
}

obj.printName()
obj.printName.call(obj2)


console.log("8");
/**
 * this keyword value depends on it's lexical envorment in arrow function
 */

let obj3 = {
    name: "Tushar",
    printName: function() {
        const y = ()=>{
            console.log(this.name); // tushar // point to this obj
        }
        y()
    },
}

obj3.printName() // tushar

console.log("9");

/**
 * The value of this in dom element is, it is reference to it's html element from where it get initiated
 */

console.log("10");

// in class and constructor it behave differently

class Rectangle {
    constructor(height, width) {
      this.height = height;
      this.width = width;
      console.log(this);
    }
  }

  