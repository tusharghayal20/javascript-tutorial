var score, round, dice, active,
score = [0, 0];
active = 0;
round = 0;
document.querySelector('.dice').style.display = 'none';
//document.querySelector('#current-0').textContent = dice;
//document.getElementById('current-'+active).innerHTML = dice;
//var a = document.querySelector('#score-0').textContent;
document.getElementById('score-0').innerHTML = '0';
document.getElementById('score-1').innerHTML = '0';
document.getElementById('current-0').innerHTML = '0';
document.getElementById('current-1').innerHTML = '0';

document.getElementById('btns-roll').addEventListener('click', function(){
  var dice = Math.floor(Math.random() * 6) + 1;
    document.querySelector('.dice').style.display = 'block';
    document.querySelector('.dice').textContent = dice;

    if (dice !== 1) {
        round += dice;
        document.getElementById('current-'+active).innerHTML = round;
    } else {
        active === 0? active = 1 : active = 0;
        document.getElementById('current-0').innerHTML = '0';
        document.getElementById('current-1').innerHTML = '0';
        round = 0;
        document.getElementsByClassName('current-'+active).classList.toggle('active');

    }
});

document.getElementById('btns-hold').addEventListener('click', function(){
    score[active] += round;
    document.getElementById('score-'+active).innerHTML = score[active];
    active === 0? active = 1 : active = 0;
    document.getElementById('current-0').innerHTML = '0';
    document.getElementById('current-1').innerHTML = '0';
    round = 0;
    document.getElementsByClassName('current-'+active).classList.toggle('active');
});